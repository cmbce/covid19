
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
# 21.9277    1.03406   0.583774  0.000603562   0.702352
#  474.65     1.0113    0.56646  0.000589232   0.701518

# parameters
location <- "France"
baseManual <- TRUE
resetParams <- FALSE
transfoParams <- TRUE
onLog <- FALSE
paramsToFit <- c("I0","beta","gamma","txMort","factConf")
nDays <- 200

graphics.off()
library("DataManagement")

source("basicData.R")
source("functions.R")
source("SIR_functions.R")
lancetDeaths <- readRDS("lancetDeaths.rds")

paramsAutoFile <- "paramsAuto.rds"
if(file.exists(paramsAutoFile)){
    params <- readRDS(paramsAutoFile)
}else{
    params <- list()
}

aCT <- readRDS("aCT.rds")
fullTable <- readRDS("fullTable.rds")
deathColumns <- names(fullTable)[grep("_deaths$",names(fullTable))]
deaths <- fullTable[,c("dates",deathColumns)]
popEsts <- readRDS("popEsts.rds")

# adjustement for Wuhan
deaths$Wuhan_deaths <- deaths$China_deaths*2548/3305

dataLoc <- data.frame(date=deaths$date,deaths=deaths[,paste0(location,"_deaths")])
dataLoc$deaths_day <- stats::filter(dataLoc$deaths,filter=c(1,-1),side=1)

paramsManual <- readRDS("paramsManual.rds")

if(baseManual){
    params[[location]] <- paramsManual[[location]]
}

if(is.null(paramsManual[[location]])){
    # Wuhan calibration
    paramsRef <- list(beta=0.7372431, 
                      gamma=0.2358145,
                      txMort=0.0004226308,
                      factConf= 5.441e-01)
}else{
    paramsRef <- paramsManual[[location]]
}

if(is.null(params[[location]]) || resetParams){
    params[[location]] <- paramsRef
    iMin <- min(which(!is.na(dataLoc$deaths) & dataLoc$deaths>10))
    dateMin <- dataLoc[iMin,"date"]
    params[[location]]$day0 <- as.Date(dateMin)-7
    params[[location]]$I0 <- dataLoc[iMin,"deaths"]/0.005
    if(location=="Wuhan"){
        params[[location]]$confDate <- lD[["China"]]$conf
    }else{
        params[[location]]$confDate <- lD[[location]]$conf
    }
}
params[[location]]$dayDeConf <- lD[[location]]$dayDeConf

if(is.null(params[[location]]$S0) || resetParams){
    # estPopRaw <- aCT[which(aCT$CountryOther==lD[[location]]$name),"estPop"]
    estPopRaw <- lancetDeaths$totPop[which(lancetDeaths$country==location)]
    if(!is.null(popEsts[[location]])){
        estPop <-popEsts[[location]] 
    }else if(!is.null(estPopRaw)){
        cat("Warning, using est pop from back calculation on worldometer\n")
        estPop <- estPopRaw
    }
    params[[location]]$S0 <- estPop
}
if(is.null(params[[location]]$I0) || resetParams){
    params[[location]]$I0 <- 100
}
if(resetParams){
    if(location=="Wuhan"){
        ageRiskRel <- 1
    }else{
        ageRiskWuhan <- lancetDeaths[which(lancetDeaths$country=="China"),"rateLancetRel"]
        ageRiskRel <- lancetDeaths[which(lancetDeaths$country==location),
                                   "rateLancetRel"]/ageRiskWuhan
    }
    params[[location]]$txMort <- params[["Wuhan"]]$txMort*ageRiskRel 
}
if(location=="Wuhan"){
    # temporarily forget about the "forgotten deaths"
    dataLoc <- dataLoc[1:86,]
}
curLoc <- list()
curLoc$Dn <- 0
curLoc$In <- params[[location]]$I0
curLoc$Sn <- params[[location]]$S0
curLoc$Rn <- 0

par(mfrow=c(1,4))
bilan <- list()

bilan[[location]] <- GetPlots(location,params[[location]],curLoc,nDays,plot=notKnited,deathsLoc=dataLoc)

expect_equal(BackTransfoParams(TransfoParams(params[[location]])),params[[location]])

# paramsMod <- params[["68"]] 
params[[location]] <- OptimSIR(dataLoc,params[[location]],curLoc,
                               onLog=onLog,transfoParams=transfoParams,
                               paramsToFit=paramsToFit)
curLoc$In <- params[[location]]$I0

bilan[[location]] <- GetPlots(location,params[[location]],curLoc,nDays,plot=notKnited,deathsLoc=dataLoc,seeRaw=TRUE)

paramsMod <- params[[location]]
paramsMod$dayDeConf <- lD[[location]]$dayDeConf
bilan[[location]] <- GetPlots(location,paramsMod,curLoc,nDays,plot=notKnited,deathsLoc=dataLoc,seeRaw=TRUE)


out <-bilan[[location]]
out$conf_Dn_day <- stats::filter(out$conf_Dn,filter=c(1,-1))
dev.new()
par(mfrow=c(1,2))
plot(out$date,out$conf_Dn,type="l")
points(out$date,out$deaths)

plot(out$date,out$conf_Dn_day,type="l")
points(out$date,out$deaths_day)
popHR <- params[[location]]$S0
estDepDR <- out$conf_Dn[nrow(out)]/popHR
DRLancetWorld <- lancetDeaths[which(lancetDeaths$country=="World"),"rateLancet"]
locLD <- ifelse(location=="Wuhan","China",location)
DRLancetLoc <- lancetDeaths[which(lancetDeaths$country==locLD),"rateLancet"]

out$conf_Dn_day_eqW <- out$conf_Dn_day*DRLancetWorld/DRLancetLoc/popHR

cat("Final death rate:",estDepDR,"\n")
cat("Corresponding World DR:",estDepDR*DRLancetWorld/DRLancetLoc,"\n")
saveRDS(out,file=paste0("out_",location,".rds"))

saveRDS(params,file=paramsAutoFile)

