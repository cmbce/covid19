
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
# test basique, depuis le début
library("testthat")
library("minpack.lm") # allow to use nlsLM, much faster than nls (3x for 68)

# définition du modèle triple itératif
SIRnextStep <-function(Sn,In,Rn,Dn,beta,gamma,txMort,thrIn=+Inf,factThrIn=2,hide=0){
    if(is.null(thrIn)) thrIn <- +Inf
    totPop <- Sn+In+Rn
    infected <- min(beta*(In^1)*Sn*(1-hide)^2/totPop,Sn)
    Snext <- Sn-infected
    resistant <- min(gamma*In,In)
    Inext <- In+infected-resistant    
    Rnext <- Rn+resistant
    if(is.null(In) || is.na(In)) browser()
    if(In<=thrIn){
        Dnext <- Dn+txMort*gamma*In
    }else{
        Dnext <- Dn+txMort*gamma*thrIn+factThrIn*txMort*gamma*(In-thrIn)
    }
    if(!is.finite(Snext) || !is.finite(Inext) || !is.finite(Rnext) || !is.finite(Dnext)) browser()
    if(Snext<0 || Inext<0 || Rnext<0 || Dnext<0) browser()
    if(abs((totPop - (Snext+Inext+Rnext))/totPop) > 2e-15) browser()

    return(list(Sn=Snext,In=Inext,Rn=Rnext,Dn=Dnext))
}

# test basique, depuis le début
Growth <- function(params,cur,tfin,dayConf,confFact,dayDeConf=+Inf){
    bilan <- list()
    bilan[[1]] <- data.frame(Sn=cur$Sn,In=cur$In,Rn=cur$Rn,Dn=cur$Dn,t=0)

    for(n in 1:tfin){
        # cat("n:",n,"dayConf:",dayConf,"dayDeConf:",dayDeConf,"\n")
        if(n<=dayConf || n>dayDeConf){
            beta <- params$beta
            hide <- 0
        }else{
            beta <- params$beta*confFact
            if(!is.null(params$confHide)){
                hide <- params$confHide
            }else{
                hide <- 0
            }
        }
        cur <- SIRnextStep(cur$Sn,cur$In,cur$Rn,cur$Dn,
                           beta,params$gamma,params$txMort,
                           thrIn=params$thrIn,factThrIn=params$factThrIn,hide=hide)
        cur$t <- n
        # if(hide!=0){browser()}
        bilan[[n+1]] <- data.frame(cur)
    }
    bilan <- data.frame(data.table::rbindlist(bilan))
    return(bilan)
}

GetGammaBetaR0 <- function(nd,GRinit){
    gamma<-1-0.5^(1/nd);
    beta<-GRinit+gamma-1;
    R0<-beta/gamma
    return(list(beta=beta,gamma=gamma,R0=R0))
}

PlotEvol <- function(bilan,add=FALSE,lty=2,log="y",lang="fr",prefix="",empty=FALSE,...){
    if(prefix!=""){
        names(bilan)<-gsub(paste0("^",prefix),"",names(bilan))
    }
    if(lang=="fr"){
        xlab <- "jours depuis le début de l'épidémie"
        ylab="population"
        if(log=="y"){
            ylab <- paste(ylab,"(échelle logarithmique)")
        }
    }else{
        xlab <- "days since the beginning of the epidemic"
        ylab="population"
        if(log=="y"){
            ylab <- paste(ylab,"(log scale)")
        }
    }
    if(!add){

       # plot(bilan$t,bilan$Sn,
        plot(bilan$t,bilan$Sn,
             ylim=c(1,bilan$Sn[1]),
             xlab=xlab,
             ylab=ylab,
             col="blue",type="n",lty=lty,log=log,...)
        posText <- 3
    }else{
        posText <- 1
    }
    if(!empty){
        lines(bilan$t,bilan$Sn,ylim=c(1,bilan$Sn[1]),col="blue",type="l",lty=lty)
        lines(bilan$t,bilan$In,col="red",lty=lty)
        lines(bilan$t,bilan$Rn-bilan$Dn,col="green",lty=lty)
        lines(bilan$t,bilan$Dn,col="black",type="l",lty=lty)
        if(!is.null(bilan$evolNeg)){
            browser()
            lines(bilan$t,out$evolNeg,col="orange",lty=lty)
            nEvolNeg <- round(out$evolNeg[nrow(out)],1)
        }
        if(!is.null(bilan$hosp)){
            lines(bilan$t,out$hosp,col="pink",lty=lty)
        }

        abline(h=10,col="grey")

        text(x=bilan$t[as.integer(nrow(bilan)*0.95)],y=bilan$Dn[nrow(bilan)],
             labels=round(bilan$Dn[nrow(bilan)]),pos=posText)        
    }
}

# fill dates in missing vector, check if guessed dates ok with existing ones
FillDates <-function(dates){
    dates <- as.Date(dates)
    iDate<-min(which(!is.na(dates)))
    valDate <- dates[iDate]
    iInDates <- 1:length(dates)
    datesTest <- iInDates-iInDates[iDate]+as.Date(valDate)
    expect_length(which(datesTest!=dates),0)
    return(datesTest)
}
# fits land (observation data with date and deaths column) to bilan (from Growth)
HorizFit <- function(land,bilan,minDeaths=5){
    # we match according to the points above minDeaths 
    iMatchs <- which(land$deaths>minDeaths)
    if(length(iMatchs)<0){
        # calcul de l'air sous la courbe observée (pas en log)
        areaObs <- 0
        for(iDayInMatch in 2:length(iMatchs)){
            iDay<- iMatchs[iDayInMatch]
            iPrev <- iMatchs[iDayInMatch-1]
            nD <- as.integer(land$date[iDay]-land$date[iPrev])
            areaObs <- areaObs+nD*(land$deaths[iDay]+land$deaths[iPrev])/2
        }
        # match de l'aire sous la courbe à l'aire sous la courbe dans raw
        ## compute sliding window of the size of iMatchs
        areasRaw <- stats::filter(bilan$Dn,filter=rep(0.5,2),method="convolution",sides=1)
        nDaysMatch <- as.integer(max(land$date[iMatchs])-min(land$date[iMatchs]))+1
        areasMatch <- stats::filter(areasRaw,filter=rep(1,nDaysMatch),
                                          method="convolution",sides=1)
        iBestMatch <- which.min(abs(areasMatch-areaObs))

        tMatch <- bilan$t[iBestMatch]
        land$t <- 1:nrow(land)-(nrow(land)-tMatch)
    }else if(length(iMatchs)<1){
        stop("No data to fit")
    }else{
        iMatchs <- iMatchs[length(iMatchs)] # useless but for debug
        # ajustement au point le plus haut
        if(land$deaths[iMatchs]<8){
            cat("The number of deaths is too low for a good fit\n")
        }
        fitDeaths <- land$deaths[iMatchs]
        iBestMatch <- which.min(abs(bilan$Dn-fitDeaths))
        tMatch <- bilan$t[iBestMatch]
        land$t <- 1:nrow(land)-(nrow(land)-tMatch)
    }

    # bilanG <- merge(land,bilan,by="t",all.x=TRUE,all.y=TRUE)
    bilanG <- merge(land,bilan,by="t",all.y=TRUE)
    bilanG$date <- FillDates(bilanG$date)

    # plot(bilanG$date,bilanG$Dn,ylim=c(0,100))
    # lines(bilanG$date,bilanG$deaths)
    return(bilanG)
}

GetPlots <-function(country,params,cur,tfin,plot=TRUE,main=NULL,seeRaw=FALSE,deathsLoc=NULL,...){
    # si conf milieu, ajuste sur bilanRaw pré-conf puis récupère bilanConf
    # si conf avant, ajuste sur bilanConf
    # si conf après (ou absent), ajuste sur bilanRaw
    if(is.null(main)){
        main<-country
    }

    # données du pays
    if(is.null(deathsLoc)){
        # use international data
        names(deaths)<- gsub("_deaths$","",names(deaths))
        deathsLoc <- data.frame(date=deaths$dates,
                           deaths=deaths[,country])
    }

    # is conf in the middle of our dataset ?
    firstNotNAday <- deathsLoc$date[min(which(deathsLoc$deaths>7))]
    # firstNotNAday <- deathsLoc$date[min(which((!is.na(deathsLoc$deaths))))]
    lastNotNAday <- deathsLoc$date[max(which((!is.na(deathsLoc$deaths))))]
    hasConf <- !is.null(params$confDate)
    confAfterStart <- params$confDate >= firstNotNAday 
    confBeforeEnd <- params$confDate<= lastNotNAday

    confMiddle <- hasConf && confAfterStart && confBeforeEnd
    if(hasConf){
        # first see where it would go without conf
        factConf <-1 
        bilanRaw <- Growth(params,cur,tfin,+Inf,factConf)
        confBefore <- !confAfterStart

        if(!is.null(params$day0)){
            dayConf <- as.integer(as.Date(params$confDate)-as.Date(params$day0))
            if(!is.null(params$dayDeConf)){
                dayDeConf <- dayConf + as.integer(as.Date(params$dayDeConf)-as.Date(params$confDate))
            }else{
                dayDeConf <- +Inf
            }
            bilanConf <- Growth(params,cur,tfin,dayConf,params$factConf,dayDeConf)
            bilanConf$date <- NA
            bilanConf$date[1] <- as.character(as.Date(params$confDate)-dayConf)
            # bilanConf$date[which(bilanConf$t==dayConf)] <- params$confDate
            bilanConf$date <- FillDates(bilanConf$date)
            bilanDate<- "conf_date"

        }else if(confMiddle){
            # then adjust to data
            landBefConf <- deathsLoc[1:which(deathsLoc$date==params$confDate),]
            bilanRaw <- HorizFit(landBefConf,bilanRaw)

            # impact du confinement
            factConf <- params$factConf

            dayConf <- bilanRaw$t[which(bilanRaw$date==params$confDate)]
            if(!is.null(params$dayDeConf)){
                dayDeConf <- dayConf + as.integer(as.Date(params$dayDeConf)-as.Date(params$confDate))
            }else{
                dayDeConf <- +Inf
            }

            bilanConf <- Growth(params,cur,max(tfin,bilanRaw$t),dayConf,factConf,dayDeConf)
            bilanDate<- "raw_date"
        }else if(confBefore){
                bilanConf <- Growth(params,cur,tfin,0,params$factConf,+Inf)
                bilanConf <- HorizFit(deathsLoc,bilanConf)

                # add dates to raw by matching on deaths with raw
                deathsConf <- bilanConf$Dn[which(bilanConf$date==params$confDate)]
                iConfRaw <- which.min(abs(bilanRaw$Dn-deathsConf))
                bilanRaw$date <- NA
                bilanRaw$date[iConfRaw] <- params$confDate
                bilanRaw$date <- FillDates(bilanRaw$date)

                # reset dates on bilanConf
                bilanConfInit <- bilanConf

                dayConf <- bilanRaw$t[which(bilanRaw$date==params$confDate)]
                if(!is.null(params$dayDeConf)){
                    dayDeConf <- dayConf + as.integer(as.Date(params$dayDeConf)-as.Date(params$confDate))
                }else{
                    dayDeConf <- +Inf
                }
                bilanConf <- Growth(params,cur,max(tfin,bilanRaw$t),dayConf,params$factConf,dayDeConf)
                bilanConf <- HorizFit(deathsLoc,bilanConf)
                bilanDate<- "conf_date"
                deathsConf <- bilanConf$Dn[which(bilanConf$date==params$confDate)]
                iConfRaw <- which.min(abs(bilanRaw$Dn-deathsConf))
                bilanRaw$date <- NA
                bilanRaw$date[iConfRaw] <- params$confDate
                bilanRaw$date <- FillDates(bilanRaw$date)

        }else if(!confBeforeEnd){
            factConf <- params$factConf
            bilanRaw <- HorizFit(deathsLoc,bilanRaw)

            dayConf <- bilanRaw$t[which(bilanRaw$date==params$confDate)]
            if(!is.null(params$dayDeConf)){
                dayDeConf <- dayConf + as.integer(as.Date(params$dayDeConf)-as.Date(params$confDate))
            }else{
                dayDeConf <- +Inf
            }

            bilanConf <- Growth(params,cur,max(tfin,bilanRaw$t),dayConf,factConf,dayDeConf)
            bilanDate<- "raw_date"
        }
    }else{ # no Conf
        bilanRaw <- Growth(params,cur,tfin,+Inf,1)

        if(!is.null(params$day0)){
            bilanRaw$date <- NA
            iDay0 <- which.min(abs(bilanRaw$Dn-params$I0))
            bilanRaw$date[iDay0] <- params$day0
            bilanRaw$date <- FillDates(bilanRaw$date)
        }else{
            bilanRaw <- HorizFit(deathsLoc,bilanRaw)
        }
        dayConf <- NULL
        bilanConf <- bilanRaw
        bilanDate<- "raw_date"
    }

    # merge output
    iNT <-which(! names(bilanRaw) %in% c("t"))
    names(bilanRaw)[iNT]<-paste0("raw_",names(bilanRaw)[iNT])
    iNT <-which(! names(bilanConf) %in% c("t"))
    names(bilanConf)[iNT]<-paste0("conf_",names(bilanConf)[iNT])
    out <- merge(bilanRaw,bilanConf,by="t")
    deathsLoc$date <- as.Date(deathsLoc$date)
    out <- merge(deathsLoc,out,by.x="date",by.y=bilanDate,all.x=TRUE,all.y=TRUE)
    out$date <- FillDates(out$date)

    if(plot) PlotEvol(bilanRaw,main=main,prefix="raw_",empty=!seeRaw,...)
    if(plot) lines(out$t,out$deaths,col="darkgrey",type="p")
    if(plot){
        PlotEvol(bilanConf,add=TRUE,lty=1,main=main,prefix="conf_",...)
        abline(v=dayConf,col="grey")
    }

    return(invisible(out))
}

TransfoParams <- function(paramsLoc){
    paramsLoc <- as.list(paramsLoc)
    paramsLoc$I0 <- log(paramsLoc[["I0"]])
    paramsLoc$beta <- log(paramsLoc[["beta"]])
    paramsLoc$gamma <- log(1/(1/paramsLoc[["gamma"]]-1))
    # paramsLoc$gamma <- log(paramsLoc[["gamma"]])
    paramsLoc$txMort <- log(1/(1/paramsLoc[["txMort"]]-1))
    # paramsLoc$factConf <- log(paramsLoc[["factConf"]])
    paramsLoc$factConf <- log(1/(1/paramsLoc[["factConf"]]-1))
    return(paramsLoc)
}
BackTransfoParams  <- function(paramsLoc){
    paramsLoc <- as.list(paramsLoc)
    if(!is.null(paramsLoc$I0)){
        paramsLoc$I0 <- exp(paramsLoc$I0)
    }
    if(!is.null(paramsLoc$beta)){
        paramsLoc$beta <- exp(paramsLoc$beta)
    }
    if(!is.null(paramsLoc$gamma)){
        paramsLoc$gamma <- 1/(1+exp(-paramsLoc$gamma))
        # paramsLoc$gamma <- exp(paramsLoc$gamma)
    }
    if(!is.null(paramsLoc$txMort)){
        paramsLoc$txMort <- 1/(1+exp(-paramsLoc$txMort))
    }
    if(!is.null(paramsLoc$factConf)){
        paramsLoc$factConf <- 1/(1+exp(-paramsLoc$factConf))
        # paramsLoc$factConf <- exp(paramsLoc$factConf)
    }
    return(paramsLoc)
}

# estimation function maker
MakeGetEstBetaGamma <- function(datLoc,datesKeep,paramsLoc,cur,nDays,transfoParams,onLog){
    GetEstBetaGamma <- function(I0=paramsLoc$I0,
                                beta=paramsLoc$beta,
                                gamma=paramsLoc$gamma,
                                txMort=paramsLoc$txMort,
                                factConf=paramsLoc$factConf,thrIn=+Inf){
        # initialize
        parLoc <- paramsLoc
        parLoc$thrIn <- +Inf

        parLoc$I0 <- I0 # for BackTransfoParams in particular
        parLoc$beta <- beta
        parLoc$gamma <- gamma
        parLoc$txMort <- txMort
        parLoc$factConf <- factConf

        if(transfoParams){
            parLoc <- BackTransfoParams(parLoc)
        }
        cur$In <- parLoc$I0

        # get estimate
        bilan <- GetPlots(datLoc,parLoc,cur,tfin=nDays,plot=FALSE,deathsLoc=datLoc)

        # format estimate
        # iInit <- which(bilan$date==as.Date(datesKeep[1]))
        # iEnd <- which(bilan$date==as.Date(datesKeep[2]))
        # estDeaths <- bilan$conf_Dn[iInit:iEnd]
        estDeaths <- bilan$conf_Dn[match(datesKeep,bilan$date)]

        if(onLog){
            estDeaths <- log(estDeaths+1)
        }
        return(estDeaths)
    }
    return(GetEstBetaGamma)
}

OptimSIR <- function(datLoc,paramsLoc,cur,onLog=FALSE,transfoParams=FALSE,
                     paramsToFit=c("I0","beta","gamma","txMort","factConf"),maxiter=100){

    # determine usable data
    iInit1 <- min(which(!is.na(datLoc$deaths)))
    iDeathsNeg <- which(datLoc$deaths<0)
    if(length(iDeathsNeg)>0){
        iInit2 <- max(iDeathsNeg)+1
    }else{
        iInit2 <- -Inf
    }
    iInit3 <- min(which(datLoc$deaths>5))
    iInit <- max(iInit1,iInit2,iInit3)
    iEnd <- max(which(!is.na(datLoc$deaths) ))
    datesKeep <- range(datLoc$date[iInit:iEnd])
    # paramsLoc$day0 <- datesKeep[1]
    obs <- datLoc[iInit:iEnd,c("date","deaths")]

    if(onLog){
        obs$deaths <- log(obs$deaths+1)
    }

    paramsLoc$I0 <- cur$In # necessary for TransfoParams and simplify the rest

    # setting the bounds
    lowerBounds <- c(I0=1e-9,beta=1e-2,gamma=1e-2,txMort=1e-6,factConf=0)
    # upperBounds <- c(+Inf,+Inf,+Inf,1,1)
    upperBounds <- c(I0=+Inf,beta=1e10,gamma=1,txMort=1,factConf=1)

    if(transfoParams){
        paramsLoc <- TransfoParams(paramsLoc)
        cur$In <- paramsLoc$I0
        lowerBounds <- unlist(TransfoParams(lowerBounds))
        upperBounds <- unlist(TransfoParams(upperBounds))
    }
    fullSetOfParams <- c("I0","beta","gamma","txMort","factConf")
    names(lowerBounds) <- fullSetOfParams
    names(upperBounds) <- fullSetOfParams

    lowerBounds <- lowerBounds[paramsToFit]
    upperBounds <- upperBounds[paramsToFit]

    lowerBounds <- lowerBounds[paramsToFit]
    upperBounds <- upperBounds[paramsToFit]

    GetEstBetaGamma <- MakeGetEstBetaGamma(datLoc,obs$date,paramsLoc,cur,nDays,transfoParams,onLog)

    cat("Starting test")
    # test the optimization function

    paramFormula <- paste(paramsToFit,"=",paramsToFit,collapse=",")
    obsComp <- obs$deaths 
    formulaFit <- paste0("obsComp ~ GetEstBetaGamma(",paramFormula,")")

    est1 <- GetEstBetaGamma(I0=paramsLoc$I0,beta=paramsLoc$beta,
                            gamma=paramsLoc$gamma,txMort=paramsLoc$txMort,
                            factConf=paramsLoc$factConf)

    # est1 <- GetEstBetaGamma(eval(parse(text=paramFormula)))
    # bilan <- GetPlots(datLoc,paramsLoc,cur,tfin=nDays,plot=TRUE,deathsLoc=datLoc)
    plot(obsComp,est1)
    abline(a=0,b=1)

    cat("Starting nls")
    nlc <- nls.lm.control(maxiter = maxiter)

    startVals <- paramsLoc[paramsToFit]

    for(namePar in names(startVals)){
        if(!is.finite(startVals[[namePar]])){
            up <- min(1e20,upperBounds[[namePar]])
            down <- max(-1e20,lowerBounds[[namePar]])
            startVals[[namePar]] <- median(c(up,down))
        }
    }
    mod <- nlsLM(eval(parse(text=formulaFit)),
                 algorithm="port",
                 start=startVals,
                 lower=lowerBounds,upper=upperBounds,trace=TRUE,control=nlc)

    paramsMod <- paramsLoc
    for(nameParam in names(coef(mod))){
        paramsMod[[nameParam]] <- coef(mod)[[nameParam]]
    }
    paramsMod$thrIn <- +Inf
    
    if(transfoParams){
        paramsMod <- BackTransfoParams(paramsMod)
    }
    attr(paramsMod,"mod") <- mod
    attr(paramsMod,"onLog") <- onLog
    attr(paramsMod,"transfoParams") <- transfoParams

    return(paramsMod)
}
DrawsParams <- function(n,mod){
    V <- vcov(mod)
    draws <- data.frame(MASS::mvrnorm(n=n,mu=coef(mod),Sigma=V))
    if(attr(params[[location]],"transfoParams")){
        draws <- BackTransfoParams(draws)
    }
    fittable <- c("I0","beta","gamma","txMort","factConf")
    for(parName in setdiff(fittable,names(draws))){
        draws[[parName]] <- params[[location]][[parName]]
    }

    draws$R0 <- draws$beta/draws$gamma
    draws$R0conf <- draws$factConf*draws$beta/draws$gamma
    return(draws)
}
ToDate <- function(x){
    if(!is.null(x)){
        x <- as.Date(x)
    }
    return(x)
}
CleanUpParams <- function(params){
    i <- 1
    while(i<=length(params)){
        cat("i:",i,"\n")
        if(!is.list(params[[i]])){
            params <- params[-i]
            next()
        }
        params[[i]]$day0 <- ToDate(params[[i]]$day0)
        params[[i]]$confDate <- ToDate(params[[i]]$confDate)
        params[[i]]$isConv <- attr(params[[i]],"mod")$convInfo$isConv
        params[[i]]$transfoParams <- attr(params[[i]],"transfoParams")
        i <- i+1
    }
    return(params)
}

MakePars <- function(params){
    pars <- data.frame(data.table::rbindlist(params,fill=TRUE))
    pars$location <- names(params)

    pars$R0 <- pars$beta/pars$gamma
    pars$betaConf <- pars$beta*pars$factConf
    pars$R0conf <- pars$betaConf/pars$gamma
    return(pars)
}
GetIDrawOK <- function(bilanAll,keepBest=0.2){
    bilanAll$fitDiff <- abs(bilanAll$conf_Dn-bilanAll$deaths)
    summaryFit <- bilanAll%>%
        group_by(iDraw)%>% 
        summarize(sumAbsDiff=sum(fitDiff,na.rm=TRUE))
    iDrawOK <- summaryFit$iDraw[summaryFit$sumAbsDiff<quantile(summaryFit$sumAbsDiff,probs=keepBest)]
    return(iDrawOK)
}

PlotBilan <- function(bilan,keepBest=1,...){
    if(class(bilan)=="list"){
        bilanAll <- data.frame(data.table::rbindlist(bilan))
    }else{
        bilanAll <- bilan
    }
    if(keepBest < 1){
        iDrawOK <- GetIDrawOK(bilanAll,keepBest)
        bilanAll <- bilanAll[which(bilanAll$iDraw %in% iDrawOK),]
    }
    # plot(range(bilanAll$date),c(0,max(bilanAll$conf_Dn,na.rm=TRUE)),type="n",
    #      xlab="date",ylab="décès")
    # for(iDraw in 1:nrow(draws)){
    #     lines(bilan[[iDraw]]$date,bilan[[iDraw]]$conf_Dn,type="p",pch=".")
    # }
    # lines(bilan[[1]]$date,bilan[[1]]$deaths,type="p",col="blue")
    # bilanAll[which(bilanAll$date==max(bilanAll$date)),

    conf_Dn <- reshape::cast(bilanAll,date~iDraw,value="conf_Dn")
    qConf_Dn <- apply(conf_Dn,1,quantile,probs=c(0.025,0.25,0.5,0.75,0.975),na.rm=TRUE)

    dates <- as.Date(colnames(qConf_Dn))
    plot(range(dates),
         c(1,max(qConf_Dn[nrow(qConf_Dn),],na.rm=TRUE)),type="n",
         xlab="date",ylab="décès",...)

    polygon(c(dates, rev(dates)), c(qConf_Dn["2.5%",], rev(qConf_Dn["97.5%",])), 
            col = 'grey90', border = NA)
    polygon(c(dates, rev(dates)), c(qConf_Dn["25%",], rev(qConf_Dn["75%",])), 
            col = 'grey80', border = NA)
    lines(dates,qConf_Dn["50%",])
    # lines(dates,qConf_Dn["2.5%",],lty=2)
    # lines(dates,qConf_Dn["97.5%",],lty=2)
    lines(bilan[[1]]$date,bilan[[1]]$deaths,type="p",col="blue")
}
GetTrajectories <-function(location,paramsLoc,curLoc,nDays,dataLoc,mod=NULL,draws=NULL,mc.cores=mc.cores,nDraws=600){
    if(is.null(draws)){
        if(is.null(mod)){
            stop("Need to specify eather draws or mod in GetTrajectories")
        }else{
            draws <- data.frame(DrawsParams(nDraws,mod))
        }
    }

    GetTrajectory <- function(iDraw,paramsLoc){
        cat("iDraw:",iDraw,"; ")
        for(nameParam in names(draws)){
            paramsLoc[[nameParam]] <- draws[iDraw,nameParam]
        }
        curLoc$In <- paramsLoc[["I0"]]
        out <- GetPlots(location,paramsLoc,curLoc,nDays,plot=FALSE,
                        deathsLoc=dataLoc,seeRaw=TRUE)
        out$iDraw <- iDraw
        return(out)
    }
    out <- mclapply(1:nrow(draws),GetTrajectory,paramsLoc=params[[location]],
             mc.cores=mc.cores)
    return(out)
}


