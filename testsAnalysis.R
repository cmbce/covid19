
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
library("dplyr")
library("sp")
library("DataManagement")
library("frenchLandscape")
source("functions.R")
source("files.R")

# force la même échelle de doublement pour les trois courbes
GetPropLogScales <- function(listVals){
    listRat <- list()
    for(iList in 1:length(listVals)){
        listRat[[iList]] <- max(listVals[[iList]],na.rm=TRUE)/min(listVals[[iList]],na.rm=TRUE)
    }
    refRat <- max(unlist(listRat))
    ranges <- list()
    for(iList in 1:length(listVals)){
        factCorrect <- sqrt(refRat/listRat[[iList]])
        ranges[[iList]] <- c(min(listVals[[iList]],na.rm=TRUE)/factCorrect,
                             max(listVals[[iList]],na.rm=TRUE)*factCorrect)
    }

    return(ranges)
}

GetRateProg <- function(y,x,initCoefWeek=10^(7/8)){
    mod <- lm(log(y)~as.integer(x))
    coefLogJour <- mod$coefficients[["as.integer(x)"]]
    coefWeek <- exp(coefLogJour*7)
    doublingTime <- log(2)/coefLogJour

    # initialement, multiplication par 10 des nouveaux cas en 6 à 9 jours et des morts en 8 jours dans tous les pays fortement touchés
    ratioInitNow <- initCoefWeek/coefWeek
    #=> 7 fois moins vite
    return(list(coefLogJour=coefLogJour,coefWeek=coefWeek,doublingTime=doublingTime,ratioInitNow=ratioInitNow))
}

tests <- read.csv2(testsFiles)
tests$rateP <- tests$P/tests$T
tests$jour <- as.Date(tests$jour)

dep <- "75"
dat <- tests[which(tests$dep==dep & tests$cl_age90==0),]
yRanges <- GetPropLogScales(dat[,c("T","P","rateP")])

datMod <- dat[(nrow(dat)-7):nrow(dat),]
GetRateProg(datMod$rateP,datMod$jour)
#=> 2.87 par semaine
#   x2 en 4.6 jours
#   2.6 fois moins rapide
#   beaucoup plus rapide que la croissance de l'Australie actuellement (14 jours pour doublement)!

GetRateProg(datMod$P,datMod$jour)
#=> 2.19 par semaine
#   x2 en 6.2 jours
#   3.42 fois moins rapide (que national init)

TestsDep <- function(){
    par(mfrow=c(1,3))
    with(dat,plot(jour,T,log="y",pch=3,main="T",ylim=yRanges[[1]],xlab=""))
    with(dat,plot(jour,P,log="y",pch=3,main="P",ylim=yRanges[[2]],xlab=""))
    with(dat,plot(jour,rateP,log="y",pch=3,main="rateP",ylim=yRanges[[3]],xlab=""))
}

testsNat<- tests %>% 
    group_by(jour,cl_age90)%>%
    summarise(P=sum(P),T=sum(T),.groups="drop")

testsNat$rateP <- testsNat$P/testsNat$T


testsNatAll <- testsNat[which(testsNat$cl_age90==0),]
yRanges <- GetPropLogScales(testsNatAll[,c("T","P","rateP")])

franceDepGroup <- readRDS("data/franceDepGroup.rds")
franceDepGroupSep <- readRDS("data/franceDepGroupSep.rds")

lastDay <- last(tests$jour)
lastDepTests <- tests[tests$jour==lastDay,]
lastWeekDepTests <- tests %>%
    filter(jour<=lastDay & jour>=(lastDay-6) & cl_age90==0)%>%
    group_by(dep)%>%
    summarize(P=sum(P),T=sum(T),pop=as.numeric(as.character(first(pop))))%>%
    ungroup()%>%
    mutate(rateP=P/T,ratePop=P/pop)

anteLastWeekDepTests <- tests %>%
    filter(jour<=(lastDay-7) & jour>=(lastDay-13) & cl_age90==0)%>%
    group_by(dep)%>%
    summarize(P=sum(P),T=sum(T),pop=as.numeric(as.character(first(pop))))%>%
    ungroup()%>%
    mutate(rateP=P/T,ratePop=P/pop)
# anteLastWeekDepTests <- merge(franceDepGroup,anteLastWeekDepTests,by.x="code_insee",by.y="dep")
anteLastWeekDepTests %<>% mutate("AL_P"=P,
                                 "AL_T"=T,
                                 "AL_rateP"=rateP)
lastWeekDepTests <- merge(lastWeekDepTests,anteLastWeekDepTests[,c("dep","AL_P","AL_T","AL_rateP")],
             by.x="dep",by.y="dep")
lastWeekDepTests %<>% mutate(rateIncP=(P-AL_P)/AL_P,
                             rateIncT=(T-AL_T)/AL_T,
                             rateIncRateP=(rateP-AL_rateP)/AL_rateP)

lastWeekDepTests <- merge(franceDepGroup,lastWeekDepTests,by.x="code_insee",by.y="dep")

# map dep
ecoleLast <- lastDepTests[which(lastDepTests$cl_age90==0),]
ecoleLast[order(ecoleLast$rateP),]
frenchDepartements$CODE_DEPT <- as.character(frenchDepartements$CODE_DEPT)
ecoleLast$dep <- as.character(ecoleLast$dep)
ecoleLast$rateP <- ecoleLast$P/ecoleLast$T
ecoleLast$pop <- as.numeric(as.character(ecoleLast$pop))
ecoleLast$ratePop <- ecoleLast$P/ecoleLast$pop
ecoleLast <- merge(frenchDepartements,ecoleLast,by.x="CODE_DEPT",by.y="dep")


MapDepTests <- function(depMap){
    # graphics.off()
    # dev.new(width=8,height=4)
    # depMap <- lastWeekDepTests
    par(mfrow=c(1,3),mar=c(0,0,4,6))
    cols <- XToColors(depMap$rateP)
    plot(depMap,col=cols,border=TRUE,
         main=paste0("Positifs par testé\ndu ",lastDay-6," au ",lastDay))
    plot(franceDepGroupSep,add=T)
    AddLegend(cols,format="g",bbox=bbox(depMap))

    cols <- XToColors(depMap$ratePop)
    plot(depMap,col=cols,border=TRUE,
         main=paste0("Positifs par habitant\ndu ",lastDay-6," au ",lastDay))
    plot(franceDepGroupSep,add=T)
    AddLegend(cols,format="g",bbox=bbox(depMap))

    cols <- XToColors(depMap$T/depMap$pop)
    plot(depMap,col=cols,border=TRUE,
         main=paste0("Testés par habitant\ndu ",lastDay-6," au ",lastDay))
    plot(franceDepGroupSep,add=T)
    AddLegend(cols,format="g",bbox=bbox(depMap))

    # mapview(ecoleLast,zcol="rateP")
}
# MapDepTests(lastWeekDepTests)

MapDepIncTests <- function(depMap){
    par(mfrow=c(1,3),mar=c(0,0,4,6))

    MakeRateMap <- function(depMap,vect,main){
        maxRange <- max(abs(range(vect)))
        cols <-XToColors(c(-maxRange,maxRange),
                         crp=colorRampPalette(c("darkgreen","green","white","red","darkred")))
        colVect <- attr(cols,"colorFun")(vect)
        plot(depMap,col=colVect,border=TRUE,
             main=main)
        plot(franceDepGroupSep,add=T)
        AddLegend(cols,format="g",bbox=bbox(depMap))
    }

    MakeRateMap(depMap,depMap$rateIncRateP,
                paste0("Augmentation des positifs\npar testé par semaine\n entre le ",
                       lastDay-6," et le ",lastDay))
    MakeRateMap(depMap,depMap$rateIncP,
                paste0("Augmentation des positifs\npar habitant par semaine\n entre le ",
                       lastDay-6," et le ",lastDay))

    MakeRateMap(depMap,depMap$rateIncT,
                paste0("Augmentation des tests\npar habitant par semaine\n entre le ",
                       lastDay-6," et le ",lastDay))

}
# MapDepIncTests(lastWeekDepTests)

# age <- 9
# whichMaxSRP <- NA
# maxSRP <- 0
# bigPb <- tests[which(tests$cl_age90==age & tests$dep=="43"),]
# plot(bigPb$jour,Smooth(bigPb$rateP),ylim=c(0,1),type="l")
# for(dep in unique(tests$dep)){
#     bigPb <- tests[which(tests$cl_age90==age & tests$dep==dep),]
#     SRP <- Smooth(bigPb$rateP)
#     lines(bigPb$jour,SRP)
#     lastSRP <-last(SRP[!is.na(SRP)])
#     if(!is.na(lastSRP)){
#         if(lastSRP>maxSRP){
#             whichMaxSRP <- dep
#             maxSRP <- last(SRP)
#         }
#     }
# }

# progrès du taux de positifs
dat <- testsNatAll[(nrow(testsNatAll)-7):nrow(testsNatAll),]
GetRateProg(dat$rateP,dat$jour)
#=> +41% par semaine
#=> x2 en 13.85 jours, similaire à la croissance de l'Australie actuellement (13 août 2020)
#=> >5 fois plus rapide initialement (comparaison à nouveaux cas /nouveaux décès)
#   

# progrès du taux de positifs
dat <- testsNatAll[(nrow(testsNatAll)-14):nrow(testsNatAll),]
GetRateProg(dat$P,dat$jour)
#=> +18 % par semaine
#=> x2 en 29 jours
#=> 6 fois moins rapide qu'initialement

# national 
TestsNational<-function(){
    testsNatAll$jour <- as.Date(testsNatAll$jour)
    par(mfrow=c(1,3))
    plot(testsNatAll$jour,testsNatAll$T,pch=".",ylab="T",
         ylim=yRanges[[1]],xlab="",
         main="Nombre quotidien de tests",log="y")
    lines(testsNatAll$jour,Smooth(testsNatAll$T,7),col="blue")
    plot(testsNatAll$jour,testsNatAll$P,pch=".",ylab="P",
         ylim=yRanges[[2]],xlab="",
         main="Nombre quotidien de tests positifs",log="y")
    lines(testsNatAll$jour,stats::filter(testsNatAll$P,filter=rep(1/7,7),
                                         method="convolution",sides=1),col="blue")
    plot(testsNatAll$jour,testsNatAll$rateP,pch=".",ylab="P/T",
         ylim=c(0,1),xlab="",
         main="Ratio positifs/total",log="")
    lines(testsNatAll$jour,stats::filter(testsNatAll$rateP,filter=rep(1/7,7),
                                         method="convolution",sides=1),col="blue")
}


# par classe d'âge
TestsNationalAges <- function(){
    par(mfcol=c(3,10),mar=c(2,2,2,0),oma=c(1,0,1,0))
    yRanges <- GetPropLogScales(testsNat[which(testsNat$cl_age90!=0),c("T","P","rateP")])
    for(cl_age90 in c(9,19,29,39,49,59,69,79,89,90)){
        dat <- testsNat[which(testsNat$cl_age90==cl_age90),]
        plot(dat$jour,dat$T,pch=".",ylab="T",xlab="",
             ylim=yRanges[[1]],
             main=paste0("age ",cl_age90,"\ntotal"),log="y")
        lines(dat$jour,Smooth(dat$T,7),col="blue")
        plot(dat$jour,dat$P,pch=".",ylab="P",xlab="",
             ylim=yRanges[[2]],
             main="positifs",log="y")
        lines(dat$jour,Smooth(dat$P,7),col="blue")
        plot(dat$jour,dat$rateP,pch=".",ylab="P/T",xlab="",
             ylim=yRanges[[3]],
             main="pos./tot.",log="y")
        lines(dat$jour,Smooth(dat$rateP,7),col="blue")
    }
    # mtext("Résultats des tests par tranche d'âge",side=3,outer=TRUE,padj=0)
}
saveRDS(testsNat,"testsNat.rds")
saveRDS(testsNatAll,"testsNatAll.rds")
saveRDS(lastWeekDepTests,"lastWeekDepTests.rds")



