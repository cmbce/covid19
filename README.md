# What it is about
Gathering and analysing Covid-19 figures daily, it produces in particular :

- a draft article : https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/publications/Covid19-1/
- an internet page : https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/covid19/

# Who is doing it ? 
## participants
- Corentin Barbu, [INRAE researcher](https://www6.versailles-grignon.inrae.fr/agronomie/CV-et-Theses/Corentin-Barbu) in epidemiology, worked 10 years on Chagas disease (human disease), currently mainly working on [pests and diseases of grain crops](https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy). [Official publications](https://scholar.google.fr/citations?user=sxDMRdQAAAAJ&hl=fr&oi=ao).
- this is an open list if you can help

## Other inputs
We thank you Michel Bertrand and Marie-Noël Mistou for early inputs on the html page.

# Content (main files)
- getStructureInt.R : computes mortality risk according to age structure for all countries in the world
- consolidatePopEstimates.R: makes one table with population estimates for all countries/regions (in process)
- updateProcess.R: process to update the data, some has to be done manually 
- summary.Rmd: html page code
- SIR.R: basic model plotting
- FitSIRint.R: fitting automatically SIR models to different countries

# TODO (YOU can help/VOUS pouvez aider)
To help you need to make a bitbucket account and contact me at my work email address (my firstname dot my lastname at my institution.fr). 

## coding
To see if you can help with the code, you should clone this repository and look eather at the summary.Rmd for html page or SIR.R for the model.
- quotidien/morts potentiels régions et pays
- densités de populations et R0
- récupérer temps qu'il fait partout dans le monde (et relier à redémarrages)
- make structure of deaths from french INSEE
- make independant the nls fit from GetPlots
- add absence of public health structure : 
    - impact on third world countries
    - overflow of public health capacities impact on deaths
- get somebody to regularly keep the numbers updated (daily)
- analysis and prediction by region

## relationship with others
- article Germany = Nouvelle Aquitaine
- derive implications of the model for different countries
    - end date of the epidemy
    - reasonable end for the lock-down
- survey of similar work by other teams to merge efforts if possible 
    - see what is done in Crowdfight COVID-19
- improve the article draft:
    - adding scientific references
    - adding the bibliography corresponding to the references (use zotero)
- increase communication
- provide answers to questions (français bienvenue)
- translating the page to English, the article to French

