
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

# source("SIR.R")
source("functions.R")
source("SIR_functions.R")
source("SIR.R")
lang <- "fr"

FranceOnlyConf <- function(){
    layout(matrix(c(1,1,2),nrow=1, byrow = TRUE))
    par(oma=c(1,1,0,0))

    paramsFrance$dayDeConf <- lD[["France"]]$dayDeConf
    bilan[["France"]] <- GetPlots("France",paramsFrance,curFrance,tfin=150,plot=TRUE,seeRaw=TRUE)


    plot(1,type="n",xlab="",ylab="",axes=FALSE,oma=c(0,0,0,0))
    legend("center",legend=c(susEst,
                             infEst,
                             resEst,
                             deathEst,
                             sansConf,
                             deathObs,
                             # "dash: without action",
                             # "plain: with public action",
                             finalEst,
                             changeName,
                             refDeathsName)
    ,col=c("blue",
           "red",
           "green",
           "black", 
           "black", 
           "black", 
           "black",
           "grey",
           "grey")
    ,lty=c(1,1,1,1,2,0,0,0,1),
    pch=c(rep("",5),"o","X","|","_"), # c("","","","","","","x","","",""),
    cex=1.1)
}
if(notKnited){
    dev.new(width=9,height=5.5)
    FranceOnlyConf()
    dev.print(pdf,"effetConfinementFrance.pdf")
}
# plot(bilan$France$date,bilan$France$conf_Dn)
# lines(bilan$France$date,bilan$France$mortsBaseCovid)
# abline(h=11500)




