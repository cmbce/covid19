# scripts to follow the development of Covid-19 epidemic

## update numbers from Worldometers (international)
source("rvestWorldometers.R") # make sure to uncomment first "UpdateAllCountriesFile()"
# il faudrait voir 
# https://www.ecdc.europa.eu/en/geographical-distribution-2019-ncov-cases

# ## update numbers on French regions
# source("covid19-fr/updateData.R")
# N'a plus l'air disponible
# sur la page source: 

## also need to download the last 
# https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/
# and update orderDataBasicPlots.R dhRef and dhIncid

# and for tests
# https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-resultats-des-tests-virologiques-covid-19/
# and update testsAnalysis.R tests


## for INSEE data (Friday nights)
# https://www.insee.fr/fr/statistiques/4487988?sommaire=4487854

# # update inseeRef in getDepAllNumbers.R
# # careful, if new month need to add something to transMonths
# # update detailINSEE in ehpad.R
# update preTraitINSEE.R folderInit

source("orderDataBasicPlots.R")
source("getDepAllNumbers.R")
source("ehpad.R",echo=T)
source("consolidationFrance.R")
# source("depOptimSIR.R")

## make basic tables and plots

## make basic international plots
source("rvestWorldometers.R")
source("autoDownSPF.R")

source("orderDataBasicPlots.R")
source("multiCountryAnalysis.R")

## render the html summary
# rmarkdown::render('summary.Rmd')
rmarkdown::render('summary.Rmd',output_file="website/index.html")
rmarkdown::render("overallECDC.Rmd",output_file="website/overallECDC.html")
rmarkdown::render("depIncid.Rmd",output_file="website/depIncid.html")

# rmarkdown::render('effetDeconfinement.Rmd')

./pushIt

# to make pdf from html
# wkhtmltopdf summary.html summary.pdf
# est-ce que ça marche avec embed=FALSE pour le html ?

## to push new article versions
# scp Covid19-1_2020-03-31.pdf Covid19-1_2020-03-31_FR.pdf spatialAgronomy:public_html/publications/Covid19-1/
