
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

params <- readRDS("params.rds")
graphics.off()
source("basicData.R")
source("functions.R")
source("SIR_functions.R")
fullTable <- readRDS("fullTable.rds")
deathColumns <- names(fullTable)[grep("_deaths$",names(fullTable))]
deaths <- fullTable[,c("dates",deathColumns)]
aCT <- readRDS("aCT.rds")

params$Wuhan$dayDeConf <- "2020-03-28"
params$France$dayDeConf <- "2020-04-22"
params$Italie$dayDeConf <- "2020-04-13"
params$Spain$dayDeConf <- "2020-04-13"
params$SKorea$dayDeConf <- "2020-04-15"
# par(mfrow=c(2,2))
# China <- GetPlots("China",params$Wuhan,curWuhan,nDays,main="Wuhan")
# GetPlots("France",params$France,curFrance,nDays,main=lD[["France"]][[nameLoc]])
# GetPlots("China",params$Wuhan,curWuhan,nDays,main="Wuhan",log="")
# 
# par(mfrow=c(2,2))
# for(dayDeConf in c("2020-03-15","2020-04-05","2020-04-15","2020-05-01")){
#     params$France$dayDeConf <- dayDeConf
#     bilan <- GetPlots("France",params$France,curFrance,nDays,main=lD[["France"]][[nameLoc]])
# }

nDays <- 120
params$France$dayDeConf <- NULL
bilanNoDeconf <- GetPlots("France",params$France,curFrance,nDays,main=lD[["France"]][[nameLoc]],plot=FALSE)
params$France$dayDeConf <- "2020-04-22"
bilanDeConf <- GetPlots("France",params$France,curFrance,nDays,main=lD[["France"]][[nameLoc]],plot=FALSE)

bilanDeConf$deaths_day <- stats::filter(bilanDeConf$deaths,filter=c(1,-1),method="convolution",
                                         sides=1)

bilanDeConf$conf_Dn_day <- stats::filter(bilanDeConf$conf_Dn,filter=c(1,-1),method="convolution",
                                         sides=1)
bilanNoDeconf$conf_Dn_day <- stats::filter(bilanNoDeconf$conf_Dn,
                                           filter=c(1,-1),method="convolution",
                                         sides=1)
# effet du confinement sur la date de pic
bilanDeConf$raw_Dn_day <- stats::filter(bilanDeConf$raw_Dn,filter=c(1,-1),method="convolution",
                                         sides=1)
dimConfPicMort <- max(bilanDeConf$raw_Dn_day,na.rm=TRUE)/max(bilanDeConf$conf_Dn_day,na.rm=TRUE)-1
cat("Diminution pic mortalité avec confinement:",dimConfPicMort,"\n")

dev.new(width=8,height=6)
par(mfrow=c(2,2),oma=c(0,0,4,0),mar=c(3,4,1,1))
plot(bilanNoDeconf$date,bilanNoDeconf$conf_Dn,type="l",lty=2,
     xlab="",ylab="Morts cumulées (échelle log)",log="y")
lines(bilanDeConf$date,bilanDeConf$conf_Dn,type="l",lty=1)
lines(bilanDeConf$date,bilanDeConf$deaths,type="p")
abline(v=as.Date(params$France$dayDeConf),col="grey",lty=2)

plot(bilanNoDeconf$date,bilanNoDeconf$conf_Dn,type="l",lty=2,xlab="",ylab="Morts cumulées")
lines(bilanDeConf$date,bilanDeConf$conf_Dn,type="l",lty=1)
lines(bilanDeConf$date,bilanDeConf$deaths,type="p")
abline(v=as.Date(params$France$dayDeConf),col="grey",lty=2)
legend("topleft",legend=c("déconfinement 22 avril","sans déconfinement","observé"),
       lty=c(1,2,0),pch=c("","","o"))

mtext("Effet du déconfinement au 22 avril 2020\néchelle log et naturelle", 
      outer = TRUE, cex = 1.5)

plot(range(bilanNoDeconf$date),range(bilanDeConf$deaths_day+1,na.rm=TRUE),type="n",
     xlab="Date",ylab="Morts par jour (échelle log)",log="y")
lines(bilanNoDeconf$date,bilanNoDeconf$conf_Dn_day,type="l",lty=2,xlab="",ylab="Morts par jour")
lines(bilanDeConf$date,bilanDeConf$conf_Dn_day,type="l",lty=1)
lines(bilanDeConf$date,bilanDeConf$deaths_day,type="b")
abline(v=as.Date(params$France$dayDeConf),col="grey",lty=2)

plot(range(bilanNoDeconf$date),range(bilanDeConf$deaths_day+1,na.rm=TRUE),type="n",
     xlab="Date",ylab="Morts par jour",log="")
lines(bilanNoDeconf$date,bilanNoDeconf$conf_Dn_day,type="l",lty=2,xlab="",ylab="Morts par jour")
lines(bilanDeConf$date,bilanDeConf$conf_Dn_day,type="l",lty=1)
lines(bilanDeConf$date,bilanDeConf$deaths_day,type="b")
abline(v=as.Date(params$France$dayDeConf),col="grey",lty=2)

dev.print(pdf,"effetDeconfinementFrance.pdf")



# par(mfrow=c(2,3))
# GetPlots("China",params$Wuhan,curWuhan,nDays,main="Wuhan")
# GetPlots("Italy",params$Italy,curItaly,nDays,main=lD[["Italy"]][[nameLoc]])
# GetPlots("Spain",params$Spain,curSpain,nDays,main=lD[["Spain"]][[nameLoc]])
# GetPlots("France",params$France,curFrance,nDays,main=lD[["France"]][[nameLoc]])
# GetPlots("SKorea",params$SKorea,curSKorea,nDays,main=lD[["SKorea"]][[nameLoc]])

