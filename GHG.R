
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

# From OECD https://stats.oecd.org/Index.aspx?DataSetCode=AIR_GHG
dat <- read.csv("AIR_GHG_23112020230952486.csv")
sel <- which(dat$Country=="France"& dat$Variable=="Total  emissions excluding LULUCF" & dat$POL=="GHG")

with(dat[sel,],plot(Year,Value))

# from https://www.macrotrends.net/countries/FRA/france/population
popFR <- read.csv("france-population-2020-11-23.csv")
popDE <- read.csv("germany-population-2020-11-23.csv")
popFR$Year <- gsub("^([0-9]{4})-[0-9]{2}-[0-9]{2}$","\\1",popFR$date)
popDE$Year <- gsub("^([0-9]{4})-[0-9]{2}-[0-9]{2}$","\\1",popDE$date)

datFR <- merge(popFR,dat[sel,c("Year","Value")],by="Year")
datFR$GHGperh <- 1000*datFR$Value/datFR$Population
sel <- which(dat$Country=="Germany"& dat$Variable=="Total  emissions excluding LULUCF" & dat$POL=="GHG")
datDE <- merge(popDE,dat[sel,c("Year","Value")],by="Year")
datDE$GHGperh <- 1000*datDE$Value/datDE$Population

plot(range(datDE$Year),range(c(0,datDE$GHGperh,datFR$GHGperh),na.rm=TRUE),type="n")
lines(datDE$Year,datDE$GHGperh,col="yellow")
lines(datFR$Year,datFR$GHGperh,col="blue")

