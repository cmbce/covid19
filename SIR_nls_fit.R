
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
# source("orderDataBasicPlots.R")
# source("multiCountryAnalysis.R")
graphics.off()
source("basicData.R")
source("SIR_functions.R")
fullTable <- readRDS("fullTable.rds")
deathColumns <- names(fullTable)[grep("_deaths$",names(fullTable))]
deaths <- fullTable[,c("dates",deathColumns)]
aCT <- readRDS("aCT.rds")

# get the distribution of GRinit (from the data)
# in France:
dF <- deaths$France_deaths
GRs<- (dF[2:nrow(deaths)])/dF[1:(nrow(deaths)-1)]
# plot(GRs)
#=> closes down clearly in the last 7 days to
GRinit <- mean(GRs[(length(GRs)-7):length(GRs)])
#-> 1.263
t.test(GRs[(length(GRs)-7):length(GRs)])
#=> à 95% entre 1.207 et 1.32

# get the distribution of nd (on en sait rien mais doit ressembler à ça
# plot(seq(0,30,0.1),dlnorm(seq(0,30,0.1),meanlog=log(5),sdlog=0.5),type="l")


cur<-list()
cur$In <- I0 <- 1
cur$Rn <- R0 <- 0
cur$Dn <- D0 <- 0

nDays <- 200
bilan <- list()
bilanEst <- list()

## Wuhan match ok
cur$Sn <- S0 <- popTotWuhan-I0-R0-D0
params <- GetGammaBetaR0(3,GRinit*1.225)
params$txMort <- 0.00042 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
params$thrIn <- +Inf
params$factThrIn <- 2
factConf <- 0.5
country <- "China"
confDate <- lD[[country]]$conf
bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)

### Wuhan Fit
GetEst <- function(nd,GR,txMort,factConf,thrIn=+Inf){
    # params <- GetGammaBetaR0(nd,GR)
    params$txMort <- txMort
    params$thrIn <- +Inf
    bilan <- GetPlots(country,params,cur,nDays,confDate,factConf,plot=FALSE)
    estDeaths <- bilan$conf_Dn
    return(log(estDeaths[iInit:length(estDeaths)]))
}
GetEstBetaGamma <- function(beta,gamma,txMort,factConf,thrIn=+Inf){
    # params <- GetGammaBetaR0(nd,GR)
    params <- list()
    params$beta <- beta
    params$gamma <- gamma
    params$txMort <- txMort
    params$thrIn <- +Inf
    bilan <- GetPlots(country,params,cur,nDays,confDate,factConf,plot=FALSE)
    estDeaths <- bilan$conf_Dn
    return(log(estDeaths[iInit:length(estDeaths)]+1))
}
## Working
iInit <- min(which(!is.na(bilan[[country]]$deaths)))
obs <- log(bilan[[country]]$deaths[iInit:length(bilan[[country]]$deaths)]+1)
mod <- nls(obs ~ GetEstBetaGamma(beta,gamma,txMort,factConf),algorithm="port",
           start=list(beta=0.737,gamma=0.23,txMort=0.0004226,factConf=0.55),
           lower=c(0,0,0,0),upper=c(1,1,1,1),trace=TRUE)

paramsMod <- list()
paramsMod$beta <- coef(mod)[["beta"]]
paramsMod$gamma <- coef(mod)[["gamma"]]
paramsMod$txMort <- coef(mod)[["txMort"]]
paramsMod$factConf <- coef(mod)[["factConf"]]
paramsMod$thrIn <- +Inf
# > beta: 0.7372431, gamma: 0.2358145, R0: 5.86996, txMort: 0.0004226308, 
# estAuto <- GetEstBetaGamma(paramsMod$beta,paramsMod$gamma,paramsMod$txMort,paramsMod$factConf)

GetEstBetaGammaFixTx <- function(beta,gamma,factConf,thrIn=+Inf){
    # params <- GetGammaBetaR0(nd,GR)
    params <- list()
    params$beta <- beta
    params$gamma <- gamma
    params$txMort <- 0.025
    params$thrIn <- +Inf
    bilan <- GetPlots(country,params,cur,nDays,confDate,factConf,plot=FALSE)
    estDeaths <- bilan$conf_Dn
    return(log(estDeaths[iInit:length(estDeaths)]+1))
}
modFix <- nls(obs ~ GetEstBetaGammaFixTx(beta,gamma,factConf),algorithm="port",
           start=list(beta=0.88,gamma=0.06,factConf=0.09),
           lower=c(0,0,0),upper=c(1,1,1),trace=TRUE,
           control=list(warnOnly=TRUE,maxiter=50))
paramsFix <- list()
paramsFix$beta <- coef(modFix)[["beta"]]
paramsFix$gamma <- coef(modFix)[["gamma"]]
paramsFix$txMort <- 0.025 
paramsFix$factConf <- coef(modFix)[["factConf"]]
paramsFix$thrIn <- +Inf

dev.new(width=15,height=5)
par(mfrow=c(1,3))
nDays <- 100
out<-GetPlots(country,paramsMod,cur,nDays,confDate,paramsMod$factConf,main="a) Best fit, low DR")
out<-GetPlots(country,paramsFix,cur,nDays,confDate,paramsFix$factConf,main="b) Best fit, DR=2.5%")
plot(1,type="n",xlab="",ylab="",axes=FALSE)
legend("center",legend=c("susceptible",
                         "cumulated deaths",
                         "Resistant",
                         "observed cumulated deaths",
                         "dash: without action",
                         "plain: with public action",
                         "final estimates of the number of deaths",
                         "grey vertical line is inflection date of public health stategy",
                         "grey horizontal corresponds to 10 deaths")
                         ,col=c("blue",
                                "red",
                                "green",
                                "black", 
                                "grey",
                                "grey",
                                "black",
                                "grey",
                                "grey",
                                "grey")
                        ,lty=c(1,1,1,1,2,1,0,1,1,1),
                        pch=c(rep("",6),"x",rep("",3)), # c("","","","","","","x","","",""),
                        cex=1.2)


dev.print(pdf,file="compFitsAuto.pdf")

# # GetEstBetaGamma(0.71,0.2,0.00042,0.5)
# iInit <- min(which(!is.na(bilan[[country]]$deaths)))
# obs <- log(bilan[[country]]$deaths[iInit:length(bilan[[country]]$deaths)]+1)
# mod <- nls(obs ~ GetEstBetaGamma(beta,gamma,txMort,factConf),algorithm="port",
#            start=list(beta=0.7,gamma=0.5,txMort=0.025,factConf=0.4),
#            lower=rep(10e-7,4),upper=rep(1-10e-7,4),trace=TRUE)
# 
# paramsMod$beta <- coef(mod)[["beta"]]
# paramsMod$gamma <- coef(mod)[["gamma"]]
# paramsMod$txMort <- coef(mod)[["txMort"]]
# factConf <- coef(mod)[["factConf"]]
# paramsMod$thrIn <- +Inf
# bilanEst[[country]] <- GetPlots(country,paramsMod,cur,nDays,confDate,factConf)
# # estAuto <- GetEstBetaGamma(paramsMod$beta,paramsMod$gamma,paramsMod$txMort,paramsMod$factConf)

# ### basic nls fit : does not converge easily
# iInit <- min(which(!is.na(bilan[[country]]$deaths)))
# obs <- log(bilan[[country]]$deaths[iInit:length(bilan[[country]]$deaths)])
# estFitManu <- GetEst(3,GRinit*1.225,0.00042,0.5)
# est <- GetEst(5,1.2*GRinit,0.00042,0.5)
# plot(obs,est)
# abline(a=0,b=1)
# txMort <- 0.025
# mod <- nls(obs ~ GetEst(nD,GR,txMort,factConf),# algorithm="port",
#            start=list(nD=3,GR=1.2,txMort=0.001,factConf=0.5),
#            lower=c(0,0,0,0),upper=c(+Inf,+Inf,1,1))
# 
# stop()
# ### check fit
# paramsMod <- GetGammaBetaR0(coef(mod)[["nD"]],coef(mod)[["GR"]])
# paramsMod$txMort <- coef(mod)[["txMort"]]
# paramsMod$thrIn <- +Inf
# factConf <- coef(mod)[["factConf"]]
# estAuto <- GetEst(coef(mod)[["nD"]],coef(mod)[["GR"]],coef(mod)[["txMort"]],factConf)
# bilanEst[[country]] <- GetPlots(country,paramsMod,cur,nDays,confDate,factConf)
# #=> not that bad

dev.new(width=18,height=10)
par(mfrow=c(2,3))
# China
GetPlots(country,paramsMod,cur,nDays,confDate,paramsMod$factConf,main="Wuhan")

## Italy match ok
estPop <- aCT[which(aCT$CountryOther==lD$Italy$name),"estPop"]
cur$Sn <- S0 <- estPop-I0-R0-D0
params <- GetGammaBetaR0(3,GRinit*1.11)
params$txMort <- 0.00042 # 0.002 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
params$thrIn <- +Inf
params$factThrIn <- 2
country <- "Italy"
confDate <- lD[[country]]$conf
factConf <- 0.67
bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)

iInit <- min(which(!is.na(bilan[[country]]$deaths)))
obs <- log(bilan[[country]]$deaths[iInit:length(bilan[[country]]$deaths)]+1)
estMan <- GetEstBetaGamma(beta=paramsMod$beta,
                gamma=paramsMod$gamma,
                txMort=paramsMod$txMort,
                factConf=paramsMod$factConf)
modItaly <- nls(obs ~ GetEstBetaGamma(beta,gamma,txMort,factConf),algorithm="port",
           start=list(beta=paramsMod$beta,
                      gamma=paramsMod$gamma,
                      txMort=paramsMod$txMort,
                      factConf=paramsMod$factConf),
           lower=c(0,0,0,0),upper=c(1,1,1,1),trace=TRUE)

bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)



## Spain match ok
country <- "Spain"
estPop <- aCT[which(aCT$CountryOther==lD[[country]]$name),"estPop"]
cur$Sn <- S0 <- estPop-I0-R0-D0
params <- GetGammaBetaR0(6,GRinit*1.2)
params$txMort <- 0.00043 # 0.002 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
params$thrIn <- 7e05
params$factThrIn <- 1
confDate <- lD[[country]]$conf
factConf <- 0.55
bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)

## France match ok
cur$Sn <- S0 <- totPopFR-I0-R0-D0
params <- list() # GetGammaBetaR0(3,GRinit*1.08)
params$gamma <- 0.23
params$beta <- 0.55
params$txMort <- 0.0004226 # 0.002 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
params$thrIn <- +Inf
params$factThrIn <- 2
country <- "France"
confDate <- lD[[country]]$conf
factConf <- 0.83
bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)

## South Korea match ok
country <- "SKorea"
estPop <- aCT[which(aCT$CountryOther==lD[[country]]$name),"estPop"]
cur$Sn <- S0 <- estPop-I0-R0-D0
params <- GetGammaBetaR0(6,GRinit*0.98)
params$txMort <- 0.00045 # 0.002 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
params$thrIn <- +Inf
params$factThrIn <- 2
confDate <- "2020-03-01" # lD[[country]]$conf
factConf <- 0.35
bilan[[country]] <- GetPlots(country,params,cur,nDays,confDate,factConf)

plot(1,type="n",xlab="",ylab="",axes=FALSE)
legend("center",legend=c("susceptible",
                         "cumulated deaths",
                         "Resistant",
                         "observed cumulated deaths",
                         "dash: without action",
                         "plain: with public action",
                         "final estimates of the number of deaths",
                         "grey vertical line is inflection date of public health stategy",
                         "grey horizontal corresponds to 10 deaths")
                         ,col=c("blue",
                                "red",
                                "green",
                                "black", 
                                "grey",
                                "grey",
                                "black",
                                "grey",
                                "grey",
                                "grey")
                        ,lty=c(1,1,1,1,2,1,0,1,1,1),
                        pch=c(rep("",6),"x",rep("",3)), # c("","","","","","","x","","",""),
                        cex=1.5)


dev.print(pdf,file="bestFitsLowDR.pdf")

# ### comparison China my fit and oms DR
# cur$Sn <- S0 <- popTotWuhan-I0-R0-D0
# country <- "China"
# confDate <- lD[[country]]$conf
# 
# dev.new(width=18,height=10)
# par(mfrow=c(1,2))
# ## best fit by hand Wuhan
# params <- GetGammaBetaR0(3,GRinit*1.225)
# params$txMort <- 0.00042 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
# params$thrIn <- +Inf
# params$factThrIn <- 2
# factConf <- 0.5
# bilan[["Wuhan"]] <- GetPlots(country,params,cur,nDays,confDate,factConf,
#                              main="Wuhan, best fit with low DR")
# 
# ## best fit Wuhan with 2.5
# cur$Sn <- S0 <- popTotWuhan-I0-R0-D0
# params <- GetGammaBetaR0(3,GRinit*1.225)
# params$txMort <- 0.025 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
# params$thrIn <- +Inf
# params$factThrIn <- 2
# factConf <- 0.35
# bilan[["Wuhan.25pc"]] <- GetPlots(country,params,cur,nDays,confDate,factConf,
#                                   main="Wuhan, best fit with OMS DR")
# dev.print(pdf,file="bestFitChinaOMSorLowDR.pdf")

# locBilan <- bilan[["Wuhan.25pc"]]
# 
# rawCols <- names(locBilan)[grep("^raw_",names(locBilan))]
# bilanRaw<- locBilan[,c("date","theorDays",rawCols)]
# names(bilanRaw)<-gsub("^raw_","",names(bilanRaw))
# bilanRaw <- DataManagement::ChangeNameCol(bilanRaw,"theorDays","t")
# PlotEvol(bilanRaw,lty=1,main="Best fit for Wuhan with OMS DR")
# 
# confCols <- names(locBilan)[grep("^conf_",names(locBilan))]
# bilanRaw<- locBilan[,c("date","theorDays",confCols)]
# names(bilanRaw)<-gsub("^conf_","",names(bilanRaw))
# bilanRaw <- DataManagement::ChangeNameCol(bilanRaw,"theorDays","t")
# PlotEvol(bilanRaw,add=TRUE)



# vérifier si l'écart entre infectés et morts match pour la corée du sud
# plotter l'observé de la France dessus
# ajuster à Wuhan 
# ajouter impact mortalité dépassement capacité système de santé 
# puis hétérogénéité de la population + ou - forte 


# plot(bilan$In/bilan$Dn)
#-> actuellement de l'ordre de 115 fois plus d'infectés que de morts

# et ça fait à la fin 1.1 millions de morts...

# test basique France
## il faut d'abord avoir une idée du passé pour connaître le nombre de susceptibles et recovered
# d'après https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology#Exact_analytical_solutions_to_the_SIR_model

# test sur Wuhan, devrait marcher si bien même biologie et transmission partout, ce qui semble être le cas d'après les courbes


 

