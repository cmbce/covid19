
########################
# 
# Script: getStructureInt.R
#
# Purpose: imports the age structure of countries
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
source("basicData.R")
library("dplyr")

# d'alain Ferer
as <- read.csv("data/structure_par_age_5_pays.csv")
#=> inutilisable à cause des virgules pour les nombres décimaux Et pour séparer les champs

hospAge <- read.csv2("data/donnees-hospitalieres-classe-age-covid19-2020-09-21-19h00.csv")
hospAge %>% filter(jour=="2020-05-17") %>% group_by(cl_age90) %>% 
    summarize(dc=sum(dc),rea=sum(rea),hosp=sum(hosp))
# cbind(rownames(pyramidsTable),pyramidsTable[,"France"])


# utilisation https://www.populationpyramid.net/china/2019/
path <- "data/2020-04-09_agePyramid"
pyramidFiles <- dir(path=path,pattern="^.*-2020\\.csv",full.names=F)
pyramids <- list()
pyramidsTable <- list()
for(file in pyramidFiles){
    country <- gsub("^(.*)-2020.csv$","\\1",file)
    raw <- read.csv(file.path(path,file)) 
    pyramids[[country]] <- raw
    raw[,country] <- raw$M+raw$F
    meltRaw<- reshape::melt(raw,id.vars="Age")
    summaryAS <- meltRaw[which(meltRaw$variable==country),]
    pyramidsTable[[country]] <- summaryAS
}
names(pyramids)
cat("missing:")
print(setdiff(names(lD),names(pyramids)))
pyramidsTable <- data.frame(data.table::rbindlist(pyramidsTable))
pyramidsTable <- reshape::cast(pyramidsTable,value="value",variable="variable")
pyramidsTable$age <- as.integer(gsub("^([0-9]+)[-+].*$","\\1",as.character(pyramidsTable$Age)))
pyramidsTable <- pyramidsTable[order(pyramidsTable$age),]
rownames(pyramidsTable) <- pyramidsTable$age
i95 <- which(pyramidsTable$age==95)
i100 <- which(pyramidsTable$age==100)
countries <- setdiff(names(pyramidsTable),c("age","Age"))
pyramidsTable[i95,countries] <- pyramidsTable[i95,countries] + pyramidsTable[i100,countries]
pyramidsTable <- pyramidsTable[1:i95,countries]


# asch<- read.csv("data/2020-04-09_agePyramid_China-2019.csv")
# it also has some morbitities, like diabetes by country
# https://www.populationpyramid.net/hnp/diabetes-prevalence-ages-20-to-79/2015/


# mortalité
# mortalité d'après article Lancet
dra <- read.csv2("data/2020-04-06_mortalityLancet.csv")
# issu de "data/en_death.rates.sex.age.xls"

dev.new(width=6,height=7)
par(mar=c(5,7,2,1))
# doesn't account for incidence
barplot(dra[,"fatalityRatio"],horiz=TRUE,names.arg=dra$ages,las=2,
        xlab="Taux de mortalité",
        main="Mortalité par classe d'age, The Lancet") 
# dev.print(pdf,"fatalityRateLancet.pdf")

# max rate deaths
lancetDeaths <- data.frame(country=countries,
                           deaths=t(pyramidsTable) %*% dra$fatalityRatio,
                           totPop=apply(pyramidsTable,2,sum))
lancetDeaths$rateLancet <- lancetDeaths$deaths/lancetDeaths$totPop
iWorld <- which(lancetDeaths$country=="World")
lancetDeaths$rateLancetRel <- lancetDeaths$rateLancet/lancetDeaths$rateLancet[iWorld]
lancetDeaths <- lancetDeaths[order(lancetDeaths$rateLancetRel),]

dev.new(height=8,width=6)

par(oma=c(1,5,0,1))
plotOnly <- c("EUROPE","AFRICA","OCEANIA","France","Italy","World","China",
              "NORTHERN-AMERICA","India","Angola",
              "SouthernAsia","SouthAmerica","Iran","Japan","CentralAmerica","Russia")
iPlot <- which(lancetDeaths$country %in% plotOnly)
barplot(lancetDeaths$rateLancetRel[iPlot],
        names.arg=lancetDeaths$country[iPlot],horiz=TRUE,las=2,
        xlab="taux de mortalité / taux de mortalité mondial",
        main="Taux de mortalités relatifs estimés \n suivant la pyramide des ages" )
# dev.print(pdf,file="relDeathRates.pdf")

cat("Ordre de mortalité croissante\n")
lancetDeaths$rLR <- round(lancetDeaths$rateLancetRel,2)
print(lancetDeaths[,c("country","rLR")])

# cat("Ordre alphabétique\n")
# lancetDeaths <- lancetDeaths[order(lancetDeaths$country),]
# print(lancetDeaths[,c("country","rateLancetRel")])

saveRDS(lancetDeaths,"lancetDeaths.rds")
saveRDS(pyramidsTable,"pyramidsTable.rds")



