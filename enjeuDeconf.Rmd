---
# compile with  rmarkdown::render('enjeuDeconf.Rmd',output_file="website/enjeuDeConf.html")
title: "Covid-19 : Quel est l'enjeu en France du déconfinement en termes de santé publique ?"
pagetitle: Enjeu déconfinement 
# output: pdf_document
output:
  html_document:
    self_contained: false
    toc: true
    toc_float: true
    theme: default
    highlight:
  pdf_document:
    keep_tex: true
---
Mis à jour : `r Sys.time()` (heure de Paris).

# Résumé
Dans cet article, je montre avec une analyse de type "règle de 3" que l'enjeu du déconfinement en France est (au bas mot) d'épargner de l'ordre de 85 000 vies humaines et d'éviter 400 000 hospitalisations d'un coup qui mettraient à nouveau notre système de santé à genoux. Bien sûr ces estimés sont à prendre avec précaution mais je justifie qu'il permettent d'évaluer la taille de l'enjeu, sur la base d'[un suivi étroit depuis le début de l'épidémie des chiffres nationaux et internationaux](https://info.spatcontrol.net/covid19/index.html).

# L'épidémie se termine dans le Haut-Rhin mais pas en France en général
L'épidémie de Covid-19, liée au virus SARS-CoV-2, est loin d'être terminée (au 25 avril, première figure). Elle a atteint un pic de mortalité autour du 1er avril 2020 mais c'est probablement en grande partie grâce au confinement qui a été mis en place le 18 mars 2020 (trait pointillé).
Les chiffres nationaux officiels sont indiqués en vert, les chiffres officiels de mortalité en hôpitaux (Santé Publique France) et la surmortalité liée au Covid-19 estimée à partir des chiffres de l'état civil communiqués par l'INSEE est en noir. La surmortalité est estimée simplement en comparant [le nombre de décès en 2020 à la même date par rapport à la moyenne des décès en 2018 et 2019](https://blogs.mediapart.fr/corentin-barbu/blog/210420/covid-19-coherence-des-chiffres-francais-officiels-totaux-hopitaux-et-insee).
```{r echo=FALSE,warning=FALSE}
source("orderDataBasicPlots.R")
```

```{r echo=FALSE,fig.height=3,fig.width=11,warning=FALSE}
dha <- data.table::fread("data/donnees-hospitalieres-covid19-2020-04-25-19h00.csv",data.table=FALSE)
dha$hospRea <- dha$hosp+dha$rea
dhaFrance <- aggregate(dha[,c("hosp","rea","rad","dc","hospRea")],by=list(sexe=dha$sexe,jour=dha$jour),sum)
dhaFranceA <- dhaFrance[which(dhaFrance$sexe==0),]

dhaFranceA$rad_day <- stats::filter(dhaFranceA$rad,filter=c(1,-1),side=1)
dhaFranceA$dc_day <- stats::filter(dhaFranceA$dc,filter=c(1,-1),side=1)
dhaFranceA$hospNetOut <- dhaFranceA$hosp-dhaFranceA$dc_day-dhaFranceA$rad_day
dhaFranceA$hosp_day <- stats::filter(dhaFranceA$hospNetOut,filter=c(1,-1),side=1)
dhaFranceA$hospArrive_day <- dhaFranceA$hosp_day+dhaFranceA$dc_day+dhaFranceA$rad_day

 depFrance <- allDeps[allDeps$Zone=="France",]
 depFrance$nExcesSimpleStartMin_day <- stats::filter(depFrance$nExcesSimpleStartMin,filter=c(1,-1),side=1)
dha68A <- dha[which(dha$dep=="68" & dha$sexe==0),]
dha68A$rad_day <- stats::filter(dha68A$rad,filter=c(1,-1),side=1)
dha68A$dc_day <- stats::filter(dha68A$dc,filter=c(1,-1),side=1)
dha68A$hospNetOut <- dha68A$hosp-dha68A$dc_day-dha68A$rad_day
dha68A$hosp_day <- stats::filter(dha68A$hospNetOut,filter=c(1,-1),side=1)
dha68A$hospArrive_day <- dha68A$hosp_day+dha68A$dc_day+dha68A$rad_day

 dep68 <- allDeps[allDeps$Zone=="Dept_68",]
 dep68$nExcesSimpleStartMin_day <- stats::filter(dep68$nExcesSimpleStartMin,filter=c(1,-1),side=1)

 natDat$deaths_day3 <- stats::filter(natDat$newDeaths,filter=rep(1/3,3),side=2)
 layout(matrix(c(1,1,2,2,3),nrow=1))
plot(depFrance[,c("date","nExcesSimpleStartMin_day")],type="l",main="France",
     ylim=c(-200,1300),ylab="nombre de décès quotidien")
lines(as.Date(dhaFranceA$jour),dhaFranceA$dc_day,type="l",col="blue")
lines(as.Date(natDat$Date),natDat$deaths_day3,col="green")
abline(v=as.Date("2020-03-18"),col="grey",lty=2)

plot(dep68[,c("date","nExcesSimpleStartMin_day")],type="l",main="Haut-Rhin",ylab="nombre de décès quotidien")
lines(as.Date(dha68A$jour),dha68A$dc_day,type="l",col="blue")
abline(v=as.Date("2020-03-18"),col="grey",lty=2)

plot(1,type="n",xlab="",ylab="",axes=FALSE)
legend("center",legend=c("chiffres officiels","hopitaux","surmortalité INSEE"),
    col=c("green","blue","black"),lty=rep(1,3))

```

En revanche, l'épidémie de Covid-19 dans le Haut-Rhin est maintenant largement terminée, du moins dans les conditions actuelles de confinement. Sur la base des données hospitalières départementales de [Santé Publique France](https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/), on peut recalculer que les nouvelles hospitalisations liées au virus sont ces derniers jours (18-25 avril) de l'ordre de 20 par jour (on est monté jusqu'à plus de 150) et les morts en hôpitaux à 3.6 (on est monté jusqu'à 50, et en moyenne plutôt 30 entre le 25 et le 31 mars). On peut donc considérer que l'épidémie s'est largement arrêtée

Cette analyse est confirmée par l'observation de la surmortalité en noir sur le graphique. La surmortalité liée au virus, ainsi estimée grâce aux chiffres INSEE est nettement plus élevée que le nombre de décès en hôpitaux (comme [déjà décrit à l'échelle de la France](https://blogs.mediapart.fr/corentin-barbu/blog/210420/covid-19-coherence-des-chiffres-francais-officiels-totaux-hopitaux-et-insee)) mais décrit exactement la même tendance : l'épidémie est pour l'instant terminée dans ce département. 

# Implication "brut de décoffrage" pour la France
Le fait que l'épidémie soit terminée permet d'évaluer le taux de mortalité de l'épidémie. En se basant sur la surmortalité calculée à partir des chiffres INSEE (il n'y a pas de chiffres officiels par département à ma connaissance), environ 1.7 personnes sont décédées pour 1000 habitants. Si l'on applique ce taux de mortalité à la France on obtient environ 112 000 morts, beaucoup plus que ce vers quoi on se dirige actuellement ([25 à 30 000 morts au moment du déconfinement](https://info.spatcontrol.net/covid19/index.html#estimation_du_taux_de_mortalité_et_prédictions_temporelles)). 

A partir des données départementales d'hospitalisations dans le Haut-Rhin, il est aussi possible de calculer qu'il y a 3 à 4 fois plus de personnes hospitalisées qui guérissent que de personnes qui meurent, cela fait au total de l'ordre de 400 000 hospitalisations, en quelques semaines, dont des dizaines de milliers en réanimation, dépassant largement la capacité actuelle de 10 000 lits en France. Comme "le gros de la vague" passerait en même temps dans plusieurs régions en cas de déconfinement brutal cela induirait bien sûr une surmortalité importante car il n'y aurait plus autant de possibilités de transferts de patients (ou de personnel médical) entre régions que ça a été le cas pour le Haut-Rhin et plus généralement le Grand-Est.

Si le Haut-Rhin est représentatif de la France, il y a donc un enjeu clair : de l'ordre de 85 000 décès de plus dans le meilleur des cas (on arrive à traiter tout le monde correctement), de l'ordre de 400 000 hospitalisations et un risque très fort de submerger les hôpitaux, non pas avec un "deuxième vague" mais avec la première qui a été largement retenue jusqu'à maintenant par le confinement. 

# Le Haut-Rhin est représentatif de la France
On sait que l'issue fatale de la maladie est très liée à l'âge des victimes. On connaît approximativement les structures d'âges des départements français et celle du Haut-Rhin induit un [facteur de risque lié à l'âge] égal à celui de la France dans son ensemble (ok, 3/1000 en dessous soit bien moins que la marge d'erreur de ce genre de calculs). 

```{r echo=FALSE,fig.height=4,fig.width=4,warning=FALSE}
bilanDep$densPop <- bilanDep$total/bilanDep$Superficiekm2
# bilanDep$densPop[which(bilanDep$dep=="68")]/bilanDep$densPop[which(bilanDep$dep=="France")]
```

Par ailleurs, le taux de mortalité dépend aussi de transmission la maladie qui dépend d'une part de la densité de la population et d'autre part des mesures de prévention. Pour la densité de la population, c'est compliqué : la France est vraiment *très* hétérogène : le Haut-Rhin est 60 fois plus densément peuplé (habitants par kilomètre carré) que la Guyane et encore 15 fois plus peuplé que la Lozère mais il est 95 fois moins peuplé que Paris et encore 4.6 fois moins peuplé que le Val d'Oise, un département relativement rural de l'Ile-de-France. 

Etant donné cette diversité, on peut considérer que le Haut-Rhin qui est seulement 2.1 fois plus peuplé que la moyenne de la France ne reflète pas trop mal l'ensemble de la France, d'autant que les variations de transmission changent avec le log de la densité de la population (analyses personnelles non publiées), un facteur 2 reste raisonable (Paris a d'ailleurs aussi bien entamé la descente, ce qui permet d'évaluer la mortalité à un peu plus d'un pour 1000 *mais avec un confinement nettement plus tôt dans l'épidémie*).

```{r echo=FALSE,fig.height=3,fig.width=3.5,warning=FALSE}
dha75A <- dha[which(dha$dep=="75" & dha$sexe==0),]
dha75A$rad_day <- stats::filter(dha75A$rad,filter=c(1,-1),side=1)
dha75A$dc_day <- stats::filter(dha75A$dc,filter=c(1,-1),side=1)
dha75A$hospNetOut <- dha75A$hosp-dha75A$dc_day-dha75A$rad_day
dha75A$hosp_day <- stats::filter(dha75A$hospNetOut,filter=c(1,-1),side=1)
dha75A$hospArrive_day <- dha75A$hosp_day+dha75A$dc_day+dha75A$rad_day

 dep75 <- allDeps[allDeps$Zone=="Dept_75",]
 dep75$nExcesSimpleStartMin_day <- stats::filter(dep75$nExcesSimpleStartMin,filter=c(1,-1),side=1)
plot(dep75[,c("date","nExcesSimpleStartMin_day")],type="l",main="Paris (75)",
     ylab="nombre de décès quotidien")
lines(as.Date(dha75A$jour),dha75A$dc_day,type="l",col="blue")
abline(v=as.Date("2020-03-18"),col="grey",lty=2)

```

Enfin, le confinement dans le Haut-Rhin a été appliqué très tard par rapport au déroulement de l'épidémie : le nombre de morts par jour était déjà très proche de son maximum le jour du confinement, ce qui indique que la stabilisation a eu lieu non grâce au confinement mais parce que l'épidémie a atteint un niveau d'équilibre sans confinement (mais avec des mesures barrières) ! Il pourrait y avoir un doute sur ce point si par ailleurs le confinement avait un effet très fort mais on voit que tant au niveau national qu'à Paris, la stabilisation n'a lieu que bien plus tard suggérant que le confinement n'a pas eu de manière générale un effet immédiat (ni extrêmement fort mais suffisant pour freiner très fortement l'épidémie dans les régions peu denses et éviter une saturation trop importante des hôpitaux). 

Vous me direz : tout ça c'est bien beau mais est-ce que vraiment l'épidémie pourrait vraiment arriver dans ma région ? Oui, [toutes les régions sont touchées maintenant](https://info.spatcontrol.net/covid19/#les_nombres_de_morts), qui plus est avec des trajectoires très parallèles pour la plupart d'entre elles (et parallèles à la trajectoire du Grand-Est) ce qui indique que l'épidémie se développe partout à peu près de la même manière, elle est juste à des stades différents, plus ou moins bloquée par le confinement.

En conclusion, parce que le Haut-Rhin a une structure de population proche de celle de la "France moyenne" et parce que le confinement a été appliqué très tard pour ce département, il donne une bonne indication de ce qui pourrait se passer si le déconfinement n'était qu'un retour immédiat "à la vie d'avant" ou plus précisément à l'application des mesures barrières en France telles qu'elles l'étaient dans le Haut-Rhin dans les 3 semaines avant le confinement national. En ce qui concerne la mortalité, on passerait en quelques semaines ou mois de 25 000 à 112 000 morts. Il s'agit donc de protéger de la mort de l'ordre de 85 000 personnes.

# Estimés à partir d'autres méthodes
Pour quelque chose d'aussi complexe, il est bien évident qu’il n’y a pas qu’une manière de voir les choses. Aucun estimé et aucune estimation ne doit être prise comme une vérité absolue, même (surtout ?) celle que j’ai présenté. Voici quelques autres calculs de coin de table à partir d'études très sérieuses.

## La mortalité théorique d’après [un article dans le journal The Lancet](https://www.thelancet.com/pdfs/journals/laninf/PIIS1473-3099(20)30243-7.pdf) 
Si l'on applique les mortalités estimées par classe d'âge dans cet article à la structure d'âge de la France on obtient [entre 200 000 et 1 million de morts, 500 000 en prenant l'estimé "central"](https://blogs.mediapart.fr/corentin-barbu/blog/030420/covid-19-le-difficile-dimensionnement-de-la-reponse), en gros 5 fois plus élevé que l'estimé ci-dessus. Cet estimé a été fait au début de l'épidémie, en Chine, dans des circonstances où personne encore ne faisait attention. De plus, ça considère que tous les gens au contact du virus développent le virus (ce qui est encore mal évalué). C'est donc probablement ce que l'on pourrait atteindre si il n'y avait véritablement aucune mesure de prise et aucune personne véritablement immune, naturellement, au virus. Dans une [étude de l'institut Pasteur](https://blogs.mediapart.fr/corentin-barbu/blog/030420/covid-19-le-difficile-dimensionnement-de-la-reponse) sur 661 personnes liées à un lycée de l'Oise, il est indiqué que si autour de 38% des étudiants du lycée étaient positifs aux tests sérologiques, seulement un tiers de leurs parents et frères et soeurs (~10%) étaient positifs. Cela montre le caractère relatif de la transmission au sein même d'une famille, au moins dans un contexte où les personnes se sont mises à "faire attention". Si l'on applique ce facteur de 3.8 à 500 000, on obtient 131 000 morts, ce qui est relativement proche des 120 000 estimés en prenant le Haut-Rhin comme référence (~10%), là encore ça ne prouve rien mais ça permet de penser que les ordres de grandeur sont compatibles dans un contexte où l'on "fait attention". Si vous avez envie de vous faire peur, vous pouvez cependant prendre les 500 000 décès comme référence haute.

## L'immunisation de la population

Une approche assez différente est de considérer quelle fraction de la population est estimée positive actuellement. Une autre étude d'institut pasteur suggère qu'étant donné le pourcentage d'immunisation de la population [une "deuxième vague" est probable](https://www.pasteur.fr/fr/espace-presse/documents-presse/covid-19-modelisation-indique-que-pres-6-francais-ont-ete-infectes). L'[étude originale](https://hal-pasteur.archives-ouvertes.fr/pasteur-02548181/document) indique d'abord que la mortalité serait de 0.53% pour les personnes infectées (avec un facteur 10000 entre les moins de 20 ans et les plus de 80 ans). Etant donné que l'épidémie devrait s'arrêter lorsque 70% de la population sera atteinte (même article), cela correspond à 244000 morts, en gros le double de l'estimé ci-dessus. Ils estiment aussi le taux d'hospitalisation à 2.6% parmi les infectés, ce qui selon le même calcul suggère 1.2 millions d'hospitalisation (en gros 3 fois l'estimé ci-dessus). Par ailleurs, toujours dans le même article, et sur la base de leur modélisation complexe, ils estiment qu'avec le déconfinement le pourcentage de la population immunisée est stabilisé actuellement autour de 5.7% de la population (en fait entre 3.5 et 10.3%). Il faudrait donc multiplier par 7 à 20 le nombre de décès actuel pour pour atteindre les 70% arrêtant l'épidémie. On retrouve de l'ordre de 170 000 à 500 000 morts. 

Personnellement je pense plus fiable de prendre comme référence la population du Haut-Rhin que la population du Diamond Princess comme ils le font mais il se pourrait aussi comme ils le suggèrent que le confinement ait eu un effet plus important qu'il n'y paraît dans cette sous-population. En tout état de cause, leur méthodologie est *beaucoup* plus complexe que celle développée ci-dessus et ce sont *eux les experts en virologie humaine*, les meilleurs d'entre nous en sont réduit à des conjectures qui cependant toutes pointes vers un nombre décès potentiel final de l'ordre de 100 000 à quelques centaines de milliers décès et plusieurs centaines de milliers d'hospitalisations en cas de retour d'un coup "à la vie normale". 

# Une bonne et une mauvaise nouvelle
La mauvaise c'est que si l'on déconfine en reprenant la vie "comme avant", le fait d'avoir confiné n'abaisse qu'assez peu le nombre de gens qui devront avoir été en contact avec le virus avant que l'épidémie ne s'arrête complétement. Sans précautions et quelles que soient les estimations précises on risque donc de voir se réaliser les estimés ci-dessus ou pire dans un système de santé mis de nouveau à genoux.

La bonne nouvelle c'est que les gestes barrières (+ masques + limiter les rencontres au maximum) ont de fortes chances non seulement de limiter la transmission mais aussi de diminuer la gravité de la maladie chez les gens qui seront en contact avec le virus : si vous êtes en contact avec de plus petites quantité de virus au moment de l'infection, votre corps a un peu plus de temps pour réagir et votre probabilité d'une maladie grave baisse. Ce n'est pas sûr [mais plusieurs experts le pense](https://info.spatcontrol.net/covid19/index.html#chargeInfectieuse).

# Comment relever le défi (avis personnels) ? 
D'abord, suivre les consignes du gouvernement : *notre* santé est l'affaire de *chacun*, nul ne peut se permettre de faire les choses à sa sauce d'après ses petites impressions. Rien de ce qui est présenté ici ne doit être interprété comme encourageant à désobéir aux consignes ou même à se défier du gouvernement. Cela ne doit cependant pas vous décourager de faire entendre votre voix, suivant vos valeurs et vos intérêts, par exemple en écrivant à votre député ou en participant au débat sur les réseaux sociaux !

Bien sûr, il faut commencer à reprendre, mais doucement. 

Pragmatiquement, au niveau individuel, les gestes barrières et la limitation des contacts resteront cruciaux pendant plusieurs mois à la fois pour limiter le nombre de personnes ayant été mises en contact avec le virus avant que l'épidémie s'éteigne et probablement pour limiter la gravité de la maladie chez les personnes qui auront été mises en contact avec le virus. 

Ensuite, collectivement, il faut minimiser les contacts entre inconnus : imposer les masques dans les transports en commun et plus généralement dans l'espace public, limitation densités dans les réunions en tout genre et dans les commerces/restaurants etc. (probablement au moins jusqu'à fin août). 

Il me semble important aussi d'éviter que la vague frappe partout en même temps et pour cela je ne vois pas d'autre solution qu'un déconfinement échelonné suivant les départements/régions. 

Enfin, parce que ce qui arrête durablement une épidémie (et à plus forte raison une pandémie) c'est avant tout le taux de personnes qui ont été en contact avec le virus, il faut exposer progressivement les fractions les moins à risque de la population. Par exemple: commencer par les maternelles et les primaires (sauf maîtres et maîtresses ou enfants de parents de plus de 50 ans) et faire reprendre les fonctions les moins "télétravaillable" par les moins de 50 ans. Ajouter 2 semaines après les collèges et lycées (les parents des étudiants sont plus agés), attendre 2 semaines. Et ainsi de suite, réouvrir très progressivement et par "type d'activité" les lieux de rencontre des jeunes (dont les parents sont clairement à risque) pour arriver aux maisons de retraites etc. toujours avec des gestes barrières.  

Le tout doit bien sûr être associé à de la surveillance en temps réel des hospitalisations et éventuellement de l'immunisation de la population pour avancer dans la sérénité, prêts à freiner si besoin était. Si tout est bien géré, ni trop confiné ni trop relaché, en 4-5 mois (d'ici août-septembre 2020) on devrait pouvoir s'en sortir tous.

Ensemble.
