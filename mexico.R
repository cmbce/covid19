
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

graphics.off()
library("DataManagement")
source("basicData.R")
source("functions.R")
source("SIR_functions.R")
fullTable <- readRDS("fullTable.rds")
multiACT <- readRDS("multiACT.rds")
mex <- multiACT[grep("Mexico",multiACT$CountryOther),]

mexDeaths <- mex[,c("date","TotalDeaths")]
mexDeaths <- ChangeNameCol(mexDeaths,"TotalDeaths","Mexico_deaths")
mexDeaths <- ChangeNameCol(mexDeaths,"date","dates")

deathColumns <- names(fullTable)[grep("_deaths$",names(fullTable))]
deaths <- fullTable[,c("dates",deathColumns)]
deaths$dates <- as.Date(deaths$dates)
deaths <- merge(deaths,mexDeaths,by="dates",all.x=TRUE)

aCT <- readRDS("aCT.rds")
if(!exists("bilan")){
    bilan <- list()
}

country <- "Mexico"
curMexico <- list()
curMexico$Dn <- 0
curMexico$Rn <- 0
curMexico$In <- 1
curMexico$Sn <- 129.2e06

paramsMexico <- list()
paramsMexico$gamma <- 0.23
paramsMexico$beta <- 0.55
paramsMexico$txMort <- 0.0004226 # 0.002 #1/57 # d'après Corée et Norvège OMS officiel 3 mars : 3.4%
paramsMexico$thrIn <- +Inf
paramsMexico$factThrIn <- 2
paramsMexico$confDate <- lD[[country]]$conf
paramsMexico$factConf <- 0.83

paramsMexico$confDate <- "2020-04-03" # "2020-04-20"
MexicoOnlyConf <- function(){
    source("lang_dict.R")
    layout(matrix(c(1,1,2),nrow=1, byrow = TRUE))
    bilan[["Mexico"]] <- GetPlots("Mexico",paramsMexico,curMexico,tfin=200,plot=TRUE,seeRaw=TRUE)

    plot(1,type="n",xlab="",ylab="",axes=FALSE)
    legend("center",legend=c(susEst,
                             infEst,
                             resEst,
                             deathEst,
                             sansConf,
                             deathObs,
                             # "dash: without action",
                             # "plain: with public action",
                             finalEst,
                             changeName,
                             refDeathsName)
    ,col=c("blue",
           "red",
           "green",
           "black", 
           "black", 
           "black", 
           "black",
           "grey",
           "grey")
    ,lty=c(1,1,1,1,2,0,0,0,1),
    pch=c(rep("",5),"o","X","|","_"), # c("","","","","","","x","","",""),
    cex=1.1)
}
if(notKnited){
    dev.new(width=8,height=5.5)
    MexicoOnlyConf()
    dev.print(pdf,"effetConfinementMexico.pdf")
}


