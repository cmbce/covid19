
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

library("plotly")

selColumns <- names(fullTable)[grep("_newCases$",names(fullTable))]
newCases <- fullTable[,c("dates",selColumns)]
names(newCases) <- gsub("_newCases$","",names(newCases))

# courbes du nombre de cas par jour
fig <- plot_ly(x= ~newCases$dates) %>%
    layout(title="Nombre de cas par jour et par pays",
           yaxis=list(title="Nombre de cas par jour",fixedrange=TRUE), # ,type="log"),
           xaxis=list(title="Date",fixedrange=TRUE)) # %>%
    # config(displayModeBar = F) 
for(name in setdiff(names(newCases),"dates")){
    # if(name=="China"){
    #     labelName <- "Wuhan"
    #     estPop <- popTotWuhan
    # }else{
        labelName <- name
        estPop <- aCT[which(aCT$CountryOther==lD[[name]]$name),"estPop"]
    # }
    smooth <- stats::filter(newCases[,name]/estPop,sides=1,filter=rep(1/5,5))
    fig <- fig %>% 
        add_trace(y=smooth, # newCases[,name],
                  mode="lines",type="scatter",
                  name=labelName)
    confDate <- lD[[name]]$conf
    if(!is.null(confDate)){
        iConfDate <- which(newCases$dates==confDate)
        fig <- fig %>% add_trace(x=as.POSIXct(confDate),
                                 y=smooth[iConfDate], # newCases[iConfDate,name],
                                 mode="markers",type="scatter",showlegend=FALSE,
                                 name=labelName,marker=list(color='rgb(3,3,3)',size=10))
    }
}
fig 

# cas par jour/pop et lisés sur 3 jours


