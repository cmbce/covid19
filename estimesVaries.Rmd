---
# compile with  rmarkdown::render('summary.Rmd')
title: Covid-19 : sur les fossés entre estimations
pagetitle: Fossés entre les estimations 
output:
  html_document:
    toc: true
    toc_float: true
  pdf_document:
    keep_tex: true
---

Mis à jour : `r Sys.time()` (heure de Paris).
<a href="https://twitter.com/CorentinBarbu?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Follow @CorentinBarbu</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Comment réconcilier des estimations de mortalité distantes d'un facteur 50 ?

Dès que l'on parle d'épidémiologie et de chiffres en temps réel, la première chose à avoir en tête est qu'une erreur d'un facteur 2 arrive très facilement. Ce n'est la faute à personnes sinon aux maladies qui vont très vites et suivent des courbes exponentielles où, par exemple pour le Covid-19, le nombre de morts double en trois à quatre jours en début d'épidémie. 

Le problème avec cette épidémie c'est que les estimations de mortalité des uns sont 60 fois plus élevés que celles des autres. Si on peut essayer de faire de la place pour 2 fois plus de monde que prévu, faire pour 60 fois plus est impossible.

Par exemple, les mortalités par classe d'âge présentées dans un article publié dans The Lancet (https://www.thelancet.com/pdfs/journals/laninf/PIIS1473-3099(20)30243-7.pdf), l'un des tout meilleurs journaux de recherche médicale, sont entre 0.65% pour les plus jeunes et 7,8% pour les plus anciens. Si l'on applique ces taux à la population française en y multipliant par le taux de personnes attrapant l'infection dans une population de 65% (moyen terme dans leur intervale de 50 à 80%), on obtient 509 000 morts pour la France (130000 en prenant l'estimation minimum possible à partir de l'article).

En revanche, si l'on en croit cet autre [article scientifique](https://www.sciencedirect.com/science/article/pii/S0924857920300972#!), publié certes dans un journal nettement moins bon mais par l'un des médecins les plus cités de la planète, le SARS-CoV-2 n'est pas plus méchant (enfin peut-être quand même 2 fois plus) que les virus d'hiver habituels. La mortalité liée à la grippe est de l'ordre de 10 000 morts pour la population française (entre 5000 et 20000).

En prenant l'estimé des uns par rapport à l'estimé des autres on obtient donc un facteur 50, et même entre les minimums "pessimistes" (130 000) et les maximums "optimistes" (20 000), on a un facteur 6.5. Vous comprenez la difficuté des pouvoirs publics pour dimensionner leur réaction à la crise ? 

D'où viennent ces écarts ? A priori de manières différentes de voir le problème. Mon expérience de 15 ans d'étude de données publiques en tous genres, depuis la maladie de Chagas en amérique latine jusqu'à l'utilisation de pesticides en passant par les épidémies de punaises de lit m'a appris que quand les chiffres disent des choses aussi différentes, il y a sans doute quelque chose à comprendre qui va nous apprendre quelque chose d'important sur la maladie/le phénomène.

Le complotisme n'est pas seulement une attitude qui dresse les gens les uns contre les autres, c'est aussi une attitude qui empêche de comprendre le monde. 

Appliquant à cette crise mon réflexe habituel, je regarde moi-même les données avec les outils les plus simples : [règles de trois](https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/covid19/#jusqu%E2%80%99o%C3%B9_%C3%A7a_ira_) et [modèle SIR](https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/publications/Covid19-1/Covid19-1_2020-03-31_FR.pdf),la base en épidémiologie. Ces outils appliqués aux chiffres Wuhan en Chine suggèrent pour la France une mortalité de l'ordre de 20 000 personnes, plutôt une très grosse grippe donc, encore une fois ils peut très bien y avoir un facteur 2 mais probablement pas le facteur 10 à 50 qu'il faudrait pour retrouver les estimés hauts. 

- effect of public health

- "ecologic falacy"

- il ne faut pas oublier que quand tout le monde est infecté, 100% des mourrants sont covid+
  aussi parce que fragile, ils choppent tout ce qui passe.

- quand tous le monde est positif au SARS-CoV-2, tous les mourrant meurrent, en partie, à cause du virus

  Application du % d'infectés au nombre de morts

- chiffres épidémilogiques
- observés en lombardie
- chiffres français
  - covid
  - augmentation de la mortalité générale
    19%

- fier de mon pays et Stéphane Salomon fait un excellent travail


