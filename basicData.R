
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

# countries dictionary
# lockdowns (conf) according to https://www.dw.com/en/coronavirus-what-are-the-lockdown-measures-across-europe/a-52905137
lD <- list(
           Australia=list(html="australia",
                       namefr="Australie",
                        name="Australia"),
           Austria=list(html="austria",
                       namefr="Autriche",
                       name="Austria",conf="2020-03-16"),
           Belgium=list(html="belgium",
                       namefr="Belgique",
                       name="Belgium",conf="2020-03-18"),
           Brazil=list(html="brazil",
                      namefr="Brésil",
                      name="Brazil"),
           Canada=list(html="canada",
                      namefr="Canada",
                      name="Canada"),
           Colombia=list(html="colombia",
                      namefr="Colombie",
                      name="Colombia"),
           China=list(html="china",
                      namefr="Chine",
                      name="China",
                      conf="2020-01-24"),
           Denmark=list(html="denmark",
                       namefr="Danemark",
                       name="Denmark"),
           France=list(html="france",totPop=67e06,
                       namefr="France",dayDeConf="2020-05-11",
                       name="France",conf="2020-03-18"),
           Germany=list(html="germany",
                       namefr="Allemagne",
                        name="Germany",conf="2020-03-22"),
           India=list(html="india",
                      namefr="Inde",
                      name="India"),
           Iran=list(html="iran",
                       namefr="Iran",
                       name="Iran"),
           Ireland=list(html="ireland",
                       namefr="Irlande",
                       name="Ireland"),
           Italy=list(html="italy",
                       namefr="Italie",
                        name="Italy",conf="2020-03-09"),
           Mexico=list(html="mexico",
                       namefr="Mexique",conf="2020-03-30",
                       name="Mexico"),
           Netherlands=list(html="netherlands",
                       namefr="Pays-bas",
                       name="Netherlands"),
           Norway=list(html="norway",
                       namefr="Norvège",
                       name="Norway"),
           Portugal=list(html="portugal",totPop=67e06,
                       namefr="Portugal",
                       name="Portugal",conf="2020-03-18"),
           Russia=list(html="russia",
                       namefr="Russie",
                       name="Russia",conf="2020-04-13"),
           SKorea=list(html="south-korea", # page in worldometer
                       namefr="Corée du sud",
                       name="S. Korea"),# name in all countries table
           Spain=list(html="spain",
                       namefr="Espagne",
                        name="Spain",conf="2020-03-14"),
           Sweden=list(html="sweden",
                       namefr="Suède",
                       name="Sweden"),
           Switzerland=list(html="switzerland",
                       namefr="Suisse",conf="2020-03-16",
                       name="Switzerland"),
           Turkey=list(html="turkey",
                       namefr="Turquie",
                       name="Turkey"),
           UK=list(html="uk",
                       namefr="Royaume-uni",
                       name="UK",conf="2020-03-23"),
           USA=list(html="us",
                       namefr="Etats-Unis",
                        name="USA")
           ) 

## death rates
allDyn <- readRDS("allDyn.rds")
totPopFR <- lD$France$totPop
# Korea raw
SK <- allDyn[["SKorea"]]
infectedTotSK <- 8162
popTotSK <- 51.47e6
deathsSK <- 75
DRKoreaRaw <- SK$deaths[nrow(SK)]/SK$cases[nrow(SK)]
DRKoreaTot <- deathsSK/popTotSK
# cat("DRKoreaRaw:",DRKoreaRaw,"->France:",DRKoreaRaw,"\n")
# cat("DRKoreaTot:",DRKoreaTot,"->France:",totPopFR*DRKoreaTot,"\n")

# Germany
Gr <- allDyn[["Germany"]]
DRGr <- Gr$deaths[nrow(Gr)]/Gr$cases[nrow(Gr)-3]

# Diamond Princess
infectedDP <- 696
testedDP <- 3618
popTotDP <- 3711 # https://www.20minutes.fr/monde/2723279-20200220-coronavirus-capitaine-diamond-princess-heros-malgre

deathsDP <- 11 # max according to https://en.wikipedia.org/wiki/2020_coronavirus_pandemic_on_cruise_ships
DRDP <- deathsDP/popTotDP
# cat("DRDP:",DRDP,"->France:",totPopFR*DRDP,"\n")

# China
deathsChina <- 4633 # March 31st, 2020, http://weekly.chinacdc.cn/news/TrackingtheEpidemic.htm

# Hubei
ch <- allDyn[["China"]]
popTotHubei <- 58.5e6 # Wuhan : 11e6
deathsHubei <-4512 # May 1st, 2020, http://weekly.chinacdc.cn/news/TrackingtheEpidemic.htm
# deathsHubei <- ch$deaths[nrow(ch)]
DRHubei <- deathsHubei/popTotHubei
# cat("DRHubei:",DRHubei,"->France:",totPopFR*DRHubei,"\n")

# Wuhan
ch <- allDyn[["China"]]
popTotWuhan <- 11e6
deathsWuhan <- 3869 # May 01, 2020, http://weekly.chinacdc.cn/news/TrackingtheEpidemic.htm
# deathsWuhan <- ch$deaths[nrow(ch)]
DRWuhan <- deathsWuhan/popTotWuhan
# cat("DRWuhan:",DRWuhan,"->France:",totPopFR*DRWuhan,"\n")

#=> extrêmement disparate...

datRatio <- readRDS("datRatio.rds")
AddDeathRates <- function(){
    AddLine <- function(rate,name){
        abline(h=rate,lty=2)
        text(labels=name,pos=3,adj=1,
             x=datRatio[1,"Date"],
             y=rate,col=1)
    }
    AddLine(DRKoreaRaw,"D. R. Korea")
    AddLine(DRGr,"D. R. Germany")
    AddLine(DRDP,"D. R. Diamond Princ.")
    # except that given Chinese isolation, China is more like hubei or even Wuhan
    AddLine(DRDP,"D. R. Diamond Princ.")
}

# Lombardia
deathsLombardia <- 8311 # 2020-04-03 # 2020-04-01:  7593
# see  https://www.statista.com/statistics/1099389/coronavirus-deaths-by-region-in-italy/
popLombardia <- 10.4e6
DRLombardia <- deathsLombardia/popLombardia
# Emilia-Romagna 


