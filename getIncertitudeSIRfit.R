
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

library("dplyr")
library("parallel")
source("SIR_functions.R")

# nameFile <- "paramsAuto_officiel_beta_gamma_I0_txMort.rds"
nameFile <- "paramsAuto_officiel_beta_factConf_gamma_I0_txMort.rds"
# nameFile <- "paramsAuto_hosp_beta_factConf_gamma_I0_txMort.rds"
# nameFile <- "paramsAuto_officiel_beta_factConf_gamma_I0.rds"

params <- readRDS(nameFile)
location <- "France"
nDraws <- 300 # number of draws
nDays <- 200
mc.cores <- 8

typeFit <- gsub("paramsAuto_([a-zA-Z]+)_.*","\\1",nameFile,perl=TRUE)
# clean parameters
params <- CleanUpParams(params)
pars <- MakePars(params)
mod <- attr(params[[location]],"mod")

# R0 confInt
draws <- data.frame(DrawsParams(nDraws,mod))

curLoc <- list()
curLoc$Dn <- 0
curLoc$Sn <- params[[location]]$S0
curLoc$Rn <- 0

allDeps <- readRDS(file="depsAllNumbers.rds")
nameZone <- gsub("^([0-9]+.*)$","Dept_\\1",location)
source("MakeDatLoc.R")


dev.new(width=16,height=9)
params[[location]]$dayDeConf <- "2020-05-11"
bilanDeConf <-GetTrajectories(location,paramsLoc=params[[location]],curLoc,nDays,dataLoc,draws=draws,
                        mc.cores=mc.cores)

paramsLoc <- params[[location]]
paramsLoc$dayDeConf <- NULL
bilanNoDeConf <- GetTrajectories(location,paramsLoc=params[[location]],curLoc,nDays,dataLoc,
                                 draws=draws,mc.cores=mc.cores)

par(mfcol=c(2,4))
curLoc$In <- params[[location]][["I0"]]
params[[location]]$dayDeConf <- "2020-05-11"
out <- GetPlots(location,params[[location]],curLoc,nDays,plot=TRUE,
                deathsLoc=dataLoc,seeRaw=TRUE)
paramsLoc <- params[[location]]
paramsLoc$dayDeConf <- NULL
curLoc$In <- params[[location]][["I0"]]
out <- GetPlots(location,paramsLoc,curLoc,nDays,plot=TRUE,
                deathsLoc=dataLoc,seeRaw=TRUE)

PlotBilan(bilanDeConf,main="With deconf")
PlotBilan(bilanNoDeConf,main="Without deconf")
PlotBilan(bilanDeConf,main="With deconf",log="y")
PlotBilan(bilanNoDeConf,main="Without deconf",log="y")
PlotBilan(bilanDeConf,main="With deconf",keepBest=0.1)
PlotBilan(bilanNoDeConf,main="Without deconf",keepBest=0.1)

saveDir <- gsub(".rds","",nameFile)
dev.print(pdf,file.path(saveDir,paste0(location,"_Incertitude.pdf")))

