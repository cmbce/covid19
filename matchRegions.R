
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
library("testthat")
library("frenchLandscape")
mappingNewOldRegions <- list("Auvergne-Rhône-Alpes"=c("Auvergne","Rhône-Alpes"),
     "Bourgogne-Franche-Comté"=c("Bourgogne","Franche-Comté"),
     "Bretagne"="Bretagne",
     "Centre-Val de Loire"="Centre",
     "Corse"="Corse",
     "Grand Est"=c("Alsace","Champagne-Ardenne","Lorraine"),
     "Hauts-de-France"=c("Nord-Pas-de-Calais","Picardie"),
     "Île-de-France"="Île-de-France",
     "Normandie"=c("Basse-Normandie","Haute-Normandie"),
     "Nouvelle-Aquitaine"=c("Aquitaine","Limousin","Poitou-Charentes"),
     "Occitanie"=c("Languedoc-Roussillon","Midi-Pyrénées"),
     "Pays de la Loire"=c("Pays de la Loire"),
     "La Réunion"="La Réunion",
     "Martinique"="Martinique",
     "Guadeloupe"="Guadeloupe", 
     "Guyane"="Guyane",
     "Provence-Alpes-Côte d'Azur"="Provence-Alpes-Côte d'Azur")

map2 <- unlist(mappingNewOldRegions)
names(map2) <- gsub("[0-9]$","",names(map2))
map3 <- iconv(toupper(map2),to="ASCII//TRANSLIT")

expect_length(setdiff(frenchDepartements$NOM_REG,map3),0)

frenchDepartements$nouvReg <- names(map2)[match(frenchDepartements$NOM_REG,map3)]

matchRegDept <- data.table::fread("data/depts2018_UTF8.txt",data.table=FALSE)



