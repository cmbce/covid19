
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)

# test world density map againts french Departements

library("raster")
library("frenchLandscape")
library("spatDataManagement")
if(!exists("initFD")){
    initFD <- frenchDepartements
}else{
    frenchDepartements <- initFD
}

allDeps <- readRDS("depsAllNumbers.rds")
bilanAdmins <- readRDS(file="bilanAdminsMaxDC.rds")

# wdp <- raster("data/populationDensity/GHS_POP_E2015_GLOBE_R2019A_54009_1K_V1_0.tif")
# wdp_fdep <-extract(wdp,frenchDepartements)
# saveRDS(wdp_fdep,file="wdp_fdep.rds")

wdp_fdep <- readRDS("wdp_fdep.rds")

frenchDepartements$estWDP <- unlist(lapply(wdp_fdep,sum,na.rm=TRUE))

# import bioclimatic variables
simAna <- readRDS("/mnt/dataDisk/Rpackages/climateanalogues/currentRefLeaflet.rds")
currentRefDF <- readRDS("/mnt/dataDisk/Rpackages/climateanalogues/currentRefDF.rds")

frenchDepartements <- ForceProjOfFirst(simAna,frenchDepartements)
climVars <- c("temp"="bio_1","prec"="bio_12","temp_sd"="bio_4")
for(iClimVar in 1:length(climVars)){
    simAna[] <- currentRefDF[,climVars[iClimVar]]
    ext <- extract(simAna,frenchDepartements)
    frenchDepartements[1:length(ext),names(climVars)[iClimVar]] <- unlist(lapply(ext,mean,na.rm=TRUE))
}
 
iDepInAllDeps <- match(paste0("Dept_",frenchDepartements$CODE_DEPT),allDeps$Zone)
frenchDepartements$pop <- allDeps[iDepInAllDeps,"pop"]
iDepInBilanAdmins <- match(frenchDepartements$CODE_DEPT,bilanAdmins$zone)
frenchDepartements$sup <- bilanAdmins[iDepInBilanAdmins,"Superficiekm2"]
frenchDepartements$densPop <- frenchDepartements$pop/frenchDepartements$sup

plot(frenchDepartements$pop,frenchDepartements$estWDP)
#=> not bad

AvgDensPerHab <- function(vect,surf){
    num <- sum((vect/surf)^2,na.rm=TRUE)
    denum <- sum(vect,na.rm=TRUE)
    return(num/denum)
}
frenchDepartements$avgDensPerHab <- unlist(lapply(wdp_fdep,AvgDensPerHab,1))
frenchDepartements$nHab <- unlist(lapply(wdp_fdep,sum,na.rm=TRUE))

plot(frenchDepartements$densPop,frenchDepartements$avgDensPerHab,log="xy")
abline(a=0,b=1)

iLasts <- which(allDeps$date==max(allDeps$date[which(!is.na(allDeps$offRatHosp))]))
finalDeaths <- allDeps[iLasts,c("date","Zone","offRatHosp")]
iDepInFinalDeaths <- match(paste0("Dept_",frenchDepartements$CODE_DEPT),finalDeaths$Zone)
frenchDepartements$lastDeaths <- finalDeaths[iDepInFinalDeaths,"offRatHosp"]
frenchDepartements$deathsPerHab <- frenchDepartements$lastDeaths/frenchDepartements$pop

FranceAvgDensPerHab <- AvgDensPerHab(unlist(wdp_fdep),1)
FranceTotPop <- sum(unlist(wdp_fdep),na.rm=TRUE)
FranceDeathsPerHab <- sum(frenchDepartements$lastDeaths)/FranceTotPop

keptCols <- c("CODE_DEPT","NOM_DEPT","X_CENTROID","Y_CENTROID",
              "estWDP","pop","nHab","avgDensPerHab","densPop","lastDeaths","deathsPerHab",
              names(climVars))
bilanFD <- merge(bilanAdmins,frenchDepartements[,keptCols],by.x="zone",by.y="CODE_DEPT")

bilanFD$X_CENTROID <- as.numeric(bilanFD$X_CENTROID)
bilanFD$Y_CENTROID <- as.numeric(bilanFD$Y_CENTROID)

par(mfrow=c(2,2))
plot(frenchDepartements$avgDensPerHab,frenchDepartements$deathsPerHab,log="xy")
text(frenchDepartements$avgDensPerHab,frenchDepartements$deathsPerHab,labels=frenchDepartements$NOM_DEPT)
points(FranceAvgDensPerHab,FranceDeathsPerHab,col="blue")

plot(frenchDepartements$densPop,frenchDepartements$deathsPerHab,log="xy")
text(frenchDepartements$densPop,frenchDepartements$deathsPerHab,labels=frenchDepartements$NOM_DEPT)

plot(bilanFD$DRcovid,bilanFD$deathsPerHab,type="n")
text(bilanFD$DRcovid,bilanFD$deathsPerHab,labels=bilanFD$NOM_DEPT,pos=3)
text(bilanFD$DRcovid,bilanFD$deathsPerHab,labels=bilanFD$zone)

plot(bilanFD$dayMaxDC,bilanFD$deathsPerHab,log="y",type="n")
text(bilanFD$dayMaxDC,bilanFD$deathsPerHab,labels=bilanFD$NOM_DEPT,pos=3)
text(bilanFD$dayMaxDC,bilanFD$deathsPerHab,labels=bilanFD$zone)

summary(lm(deathsPerHab ~ DRcovid,data=bilanFD))
summary(lm(deathsPerHab ~ avgDensPerHab,data=bilanFD))
summary(lm(deathsPerHab ~ dayMaxDC,data=bilanFD))
summary(lm(deathsPerHab ~ X_CENTROID*Y_CENTROID,data=bilanFD))

summary(lm(deathsPerHab ~ avgDensPerHab +DRcovid+dayMaxDC+X_CENTROID*Y_CENTROID,data=bilanFD))
dev.new()
plot(bilanFD$X_CENTROID*bilanFD$Y_CENTROID,bilanFD$deathsPerHab,log="y",type="n")
text(bilanFD$X_CENTROID*bilanFD$Y_CENTROID,bilanFD$deathsPerHab,labels=bilanFD$NOM_DEPT,pos=3)
text(bilanFD$X_CENTROID*bilanFD$Y_CENTROID,bilanFD$deathsPerHab,labels=bilanFD$zone)
#=> truc majeur, peut-être plutôt "sec" que basses températures qui joue
#  + rend l'effet des densités extrêmement fort
#  + dayMaxDC clairement négatif
#  + DRcovid même si négatif n'est plus significatif

#=> Il faut réussir à prendre en compte T°/humidité et à voir si explique aussi quelque chose sur les Etats Mexicains/Américains

bilanFD$deathsPerHab_corXY <- bilanFD$deathsPerHab - (1.479e-15*bilanFD$X_CENTROID*bilanFD$Y_CENTROID - 8.871e-09 * bilanFD$X_CENTROID + -2.539e-10 * bilanFD$Y_CENTROID)

bilanFD$deathsPerHab_corXY_dM_DRc <- bilanFD$deathsPerHab_corXY - (-6.562e-06*as.integer(bilanFD$dayMaxDC) -1.498e-03*bilanFD$DRcovid)

plot(bilanFD$avgDensPerHab,bilanFD$deathsPerHab_corXY,log="xy",type="n")
text(bilanFD$avgDensPerHab,bilanFD$deathsPerHab_corXY,labels=bilanFD$zone)

plot(bilanFD$avgDensPerHab,bilanFD$deathsPerHab_corXY_dM_DRc,log="xy",type="n")
text(bilanFD$avgDensPerHab,bilanFD$deathsPerHab_corXY_dM_DRc,labels=bilanFD$zone)

Norm <- function(x){
    out <- (x-mean(x,na.rm=TRUE))/sd(x,na.rm=TRUE)
    return(out)
}
Normalize <- function(dat,vars){
    for(var in vars){
        dat[,paste0(var,"_norm")] <- Norm(dat[,var])
    }
    return(dat)
}

bilanFD <- Normalize(bilanFD,c("avgDensPerHab","DRcovid","dayMaxDC","X_CENTROID","Y_CENTROID",
                               names(climVars)))
bilanFD$XY_CENTROID_norm <- Norm(bilanFD$X_CENTROID*bilanFD$Y_CENTROID)

summary(modNorm<-lm(deathsPerHab ~ avgDensPerHab_norm +DRcovid_norm+dayMaxDC_norm+
           X_CENTROID_norm+Y_CENTROID_norm+XY_CENTROID_norm,data=bilanFD))
summary(modNorm<-lm(deathsPerHab ~ avgDensPerHab_norm +DRcovidObsFrance+dayMaxDC_norm+
           X_CENTROID_norm+Y_CENTROID_norm+XY_CENTROID_norm,data=bilanFD))
#=> change à peu près rien
bilanFD$avgDensPerHab_log_norm <- Norm(log(bilanFD$avgDensPerHab +1))
summary(modNorm<-lm(deathsPerHab ~ avgDensPerHab_log_norm +DRcovid_norm+dayMaxDC_norm+
           X_CENTROID_norm+Y_CENTROID_norm+XY_CENTROID_norm,data=bilanFD))

# compare all the sd generated by X and Y to the others:
xCoef <- modNorm$coefficients[["X_CENTROID_norm"]]
yCoef <- modNorm$coefficients[["Y_CENTROID_norm"]]
xyCoef <- modNorm$coefficients[["XY_CENTROID_norm"]]

bilanFD$sdXY_norm <- bilanFD$X_CENTROID_norm*xCoef + 
    bilanFD$Y_CENTROID_norm*yCoef + 
    bilanFD$XY_CENTROID_norm* xyCoef

densHabCoef <- modNorm$coefficients[["avgDensPerHab_log_norm"]]

sd(bilanFD$sdXY_norm)/sd(densHabCoef*bilanFD$avgDensPerHab_log_norm)
#=> geography 2.23 x plus importante que la densité de population 
#   cohérent avec les différences de R2 des modèles pour chaque élément
#   sauf que 3 variables au lieu d'une...
# au 19 novembre plus que 1.64 x donc toujours important mais nettement moins

summary(modNormClim<-lm(deathsPerHab ~ avgDensPerHab_log_norm +DRcovid_norm+dayMaxDC_norm+
           temp_norm+prec_norm,data=bilanFD))
#=> not nearly as good as X,Y and XY

frenchDepartements <- merge(frenchDepartements,bilanFD[,c("zone","DRcovid","dayMaxDC")],
                            by.x="CODE_DEPT",by.y="zone")

par(mfrow=c(2,2))
plot(frenchDepartements,col=XToColors(frenchDepartements$deathsPerHab),main="Décès par hab.")
plot(frenchDepartements,col=XToColors(log(frenchDepartements$avgDensPerHab)),main="log dens.pop.")
plot(frenchDepartements,col=XToColors(frenchDepartements$DRcovid),main="DRcovidObsFrance")
plot(frenchDepartements,col=XToColors(as.integer(frenchDepartements$dayMaxDC)),main="dayMaxDC")


# need to plot dayMaxDC and traffic
traffic <- readOGR("data/traffic/TMJA2018.shp")

