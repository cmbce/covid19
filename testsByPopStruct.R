
########################
# 
# Script:  
#
# Purpose:
# Details:
#
# Author: Corentin M. Barbu (canonical corentin period barbu working at inra in fr)
# Version: 0.1
#
# Website: https://ecosys.versailles-grignon.inra.fr/SpatialAgronomy/
#
# Copyright (c) GNU GPL v2
# (Modify this template at: ~/.vim/templates/vimodel1.R)
#########################

## easy debugging options
# options(error=browser)
# options(warn=2)
library("testthat")

maxDeaths <- readRDS("maxDeaths.rds")
testsNat <- readRDS("testsNat.rds")
toBind <- maxDeaths[,c("cl_age90","totPop","ageGroups")]
toBind <- rbind(toBind,data.frame("cl_age90"=0,totPop=sum(toBind$totPop),ageGroups="total"))
tests2 <- merge(testsNat,toBind)
expect_equal(nrow(tests2),nrow(testsNat))

tests2$rateP_pop <- tests2$P/tests2$totPop
tests2 <- tests2[order(tests2$jour),]
#dev.new(width=14,height=7)
PlotTestsByAge <- function(){
    par(mfrow=c(1,2))
    ## in general population
    minY <- min(tests2$rateP_pop)*5
    maxY <- max(tests2$rateP_pop)*0.70
    plot(tests2$jour,
         seq(minY,maxY,length.out=nrow(tests2)),
         ylab="Taux de positifs dans la population de cet age",
         xlab="",
         log="y",
         main="Positifs parmi la classe d'age\nlissage 7 jours",
         type="n")
    # vacances
    low <- minY*0.9
    high <- maxY*1.1
    polygon(x=as.Date(c("2020-12-19","2021-01-03","2021-01-03","2020-12-19")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2020-10-17","2020-11-02","2020-11-02","2020-10-17")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2020-07-06","2020-09-01","2020-09-01","2020-07-06")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2021-02-06","2021-03-07","2021-03-07","2021-02-06")),
            y=c(low,low,high,high),border="grey",lty=2)

    # classes d'âges
    ages <- unique(tests2$cl_age90)
    colAges <- c("black",topo.colors(length(ages)-1)) # paste0("gray",ages)
    ageGroups <- tests2[match(ages,tests2$cl_age90),"ageGroups"]
    iAges <- 1:length(ages)
    for(iAge in iAges){
        age <- ages[iAge]
        sel <- which(tests2$cl_age90==age)
        x <- tests2$jour[sel]
        y <- Smooth(tests2$rateP_pop[sel])
        colAge <- colAges[iAge]
        lines(x,y,col=colAge,lty=iAge)
        # text(Last(x),Last(y),age,col=colAge,pos=4)
    }
    legend("topleft",lty=1:length(ages),legend=ageGroups,col=colAges)

    ## in tested population
    minY <- min(tests2$rateP)/10
    maxY <- max(tests2$rateP)*0.9
    plot(tests2$jour,
         seq(minY,maxY,length.out=nrow(tests2)),
         ylab="Taux de positifs parmi les testés",
         xlab="",
         main="Positifs parmi les testés\nlissage 7 jours",
         type="n")
    # vacances
    low <- minY*0.9
    high <- maxY# *1.1
    polygon(x=as.Date(c("2020-12-19","2021-01-03","2021-01-03","2020-12-19")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2020-10-17","2020-11-02","2020-11-02","2020-10-17")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2020-07-06","2020-09-01","2020-09-01","2020-07-06")),
            y=c(low,low,high,high),border="grey")
    polygon(x=as.Date(c("2021-02-06","2021-03-07","2021-03-07","2021-02-06")),
            y=c(low,low,high,high),border="grey",lty=2)

    for(iAge in iAges){
        age <- ages[iAge]
        sel <- which(tests2$cl_age90==age)
        x <- tests2$jour[sel]
        y <- Smooth(tests2$rateP[sel])
        colAge <- colAges[iAge]
        lines(x,y,col=colAge,lty=iAge)
        # text(Last(x),Last(y),age,col=colAge,pos=4)
    }
    legend("topleft",lty=1:length(ages),legend=ageGroups,col=colAges)
}



